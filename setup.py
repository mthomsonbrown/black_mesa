#!/usr/bin/env python
"""Setup procedures for the akm client menu."""
from pathlib import Path
from setuptools import setup, find_packages
from glob import glob

__author__ = 'Mike Brown'

MAJ = 1
MIN = 3
MIC = 1
BLD = 0  # don't modify

BUILD_INFO = Path('build.info')
if BUILD_INFO.exists():
    with open(BUILD_INFO, 'r') as file_:
        BLD = file_.read()

setup(
    name='black-mesa',
    version=f'{MAJ}.{MIN}.{MIC}{BLD}',
    description='Test libraries',
    packages=find_packages(),
    package_data={
        'black_mesa': [
            'infrastructure/**/*.yaml',
            'infrastructure/**/*.tf',
            'infrastructure/**/*.cfg',
            'tests/**/*.yaml'
        ]
    },
    include_package_data=True,
    entry_points={
        'console_scripts': ['black-mesa = black_mesa.__main__:main']
    },
    install_requires=[
        'azure>=4.0.0,<5.0.0',
        'boto3',
        'paramiko',
        'gitpython',
        'polling',
        'pyyaml',

        'pytest',
        'pytest-coverage',
        'pytest-django',
        'pytest-mock',
        'pytest-xdist',

        'pylint',
        'pylama',

        'stdeb',
    ],
    python_requires='>=3.6',
)
