Black Mesa
===========

This is a facility covering a wide spectrum of both server and client
functional tests, as well as build scripts for various python projects,
and tooling to aid in manual testing.

Terms
=====

Test Client
    The system that Black Mesa is installed on, and tests are executed from.

Installation
=============

To install black mesa, navigate to the source folder and run :code:`pip3 install -e .`

TODO: Add all dependencies to setup.py, including pytest plugins.

Configuration
==============

black mesa.json
~~~~~~~~~~~~~~~~
Most functions performed by black mesa will require a json config file to be
populated. If a function requiring this file is run and one is not available,
a blank template will be generated for you, the operation will error out, and
you will be provided with some instructions on how to proceed.

Ansible/Terraform
~~~~~~~~~~~~~~~~~~
Newer tests use terraform and ansible to establish the
infrastructure for the tests. To use these tools, extra steps have to be taken to
establish credentials to access the various cloud resources.

AWS
++++

Two environment variables need to be set on the test client:

- export AWS_ACCESS_KEY_ID=<The access key>
- export AWS_SECRET_ACCESS_KEY=<The access key>

Azure
++++++

TODO

VSphere
++++++++

TODO

IBM
++++

TODO

Manual Operation
=================

Black Mesa can be used to provision akm servers on various cloud
platforms for use in manual testing. It can also be used to extract pki from
running systems, and configure it for use by client libraries, such as
the python sdk and akmadminc.

Provisioning Systems
~~~~~~~~~~~~~~~~~~~~~

There's a test module called test_manual_tools.py that contains various test
methods that can be invoked to spin up akms. As an example, to create an aws
akm, execute the command :code:`python3 -m pytest -s -m init`.

The methods can serve as a scratch environment for optionally uploading
binaries, files, or performing operations prior to manual intervention.

Client Libraries
~~~~~~~~~~~~~~~~~

Downloading and configuring PKI can be a somewhat involved process, so in some
situations it may be easier to use this tool to do it for you.

Python SDK
++++++++++++

To get reference to the python sdk, first instantiate an AKMController object,
then reference its python_sdk_controller element. From there you can
access the client, admin, or encryption apis.

Example:

.. code-block:: python

    from black_mesa.akm_controller import AKMController

    akm = AKMController('1.1.1.1', key='path/to/key', password='password_if_no_key')
    admin = akm.python_sdk_controller.akm_admin
    admin.clear_db()

There's also an example in the test_manual_tools.py module called test_get_key,
and other operations can be added to that module as needed.

AKMAdminC
++++++++++

TODO

Build Tools
============

Black Mesa currently builds the new python libraries that target python3.

TODO: Add build instructions...

Testing
========

Tests are all written in pytest, and can be invoked from the command line like
any other pytest suite. Common usage would be :code:`python3 -m pytest -s -m regression`.

Since several test cases involve spinning up infrastructure from scratch, many of
the suites support multiprocess execution via the xdist plugin. To run multiple tests at
once you can supply the `-n` flag with a number of executors. Keep in mind there are
limitations to the number of cloud resources that can be used at one time, so this may
fail if `n` is set too large.

Kubernetes
~~~~~~~~~~~

At the time of writing, our test container deployment is hosted on dockerhub in a private
repository. In order to give kubernetes and associated tests access to this repository, a
config file will need to be generated on the test client.  To do this:

- Install docker on the test client.
- Run :code:`docker login` to establish a session with docker hub
    - This will create a file: `~/.docker/config.json`

Ansible will copy this config file onto the cluster and use it to authenticate to our test
repository.
