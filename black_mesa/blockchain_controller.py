import os

from black_mesa.environment import Environment
from black_mesa.logger import Logger


class BlockchainController(object):
    """
    Setup AKM environment for blockchain and interact with services.
    """
    def __init__(self, remote):
        self.log = Logger(__name__).logger
        self.env = self.load_config()['akm']
        self.remote = remote

    def load_config(self):
        env = Environment()
        return env.load_config()

    def exec_check_bc_repo(self, cmd, ignore=None):
        """execute commands in the blockchain repository."""
        self.remote.exec_check(
            'cd {}; {}'.format(self.env['blockchain_path'], cmd), ignore)

    def install_node(self):
        self.log.debug("Adding node repository.")
        self.remote.exec_check(
            'curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -',
            ignore=(
                "The repository 'https://repos.townsendsecurity.com/ubuntu "
                "xenial Release' is not signed.")
        )
        self.log.debug("Installing node.")
        self.remote.exec_check(
            'sudo apt install nodejs -y',
            ignore=[
                'unable to initialize frontend: Dialog',
                'apt does not have a stable CLI interface. '
                'Use with caution in scripts.']
        )
        self.log.debug("Installing build-essential.")
        self.remote.exec_check(
            'sudo apt install build-essential -y',
            ignore=('apt does not have a stable CLI interface. '
                    'Use with caution in scripts.')
        )

    def install_dependencies(self):
        self.log.debug("Installing dependencies.")
        self.exec_check_bc_repo('npm i', ignore=' ')
        self.exec_check_bc_repo('sudo cp geth /usr/bin/')

    def open_firewall(self):
        self.log.debug("Opening firewall.")
        self.remote.exec_check('sudo ufw --force enable')
        self.remote.exec_check('sudo ufw allow 30303')
        self.remote.exec_check('sudo ufw allow 3000')

    def configure_akm(self):
        self.log.debug("Configuring akm.")
        self.remote.exec_check(
            "sudo sed -i 's/PCIDSSMode=./PCIDSSMode=N/g' /etc/akm/akm.conf")

    def run_prerequisites(self):
        self.log.debug("Running prerequisites.")
        self.exec_check_bc_repo('sudo chmod +x prerequisites.sh')
        self.exec_check_bc_repo('sudo ./prerequisites.sh', ignore=' ')

    def fix_permissions(self):
        """
        Change permissions on some python files so they can be executed.

        TODO: Bug.
        """
        self.log.debug("Fixing permissions.")
        self.remote.exec_check('sudo chmod +x {}'.format(
            os.path.join(self.env['bootstrap_path'], 'bootstrap.py')
        ))

        self.remote.exec_check('sudo chmod +x {}'.format(
            os.path.join(self.env['blockchain_path'], 'scripts',
                         'akm_license.py')
        ))
