"""Cryptography done the way AKM thinks it should be done."""
from subprocess import run, PIPE
import shlex

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes


class RSAKey:

    """
    Create and access an RSA meeting AKM supported formats.

    This class is needed instead of the python cryptography module
    because AKM requires a pkcs1 serialized private key for wrapper
    key import, and cryptography only supports pkcs8+ for the private key.
    """

    def __init__(self):
        self.private = None
        self.public = None

    def generate(self):
        """
        Use openssl to generate a public private key pair.

        Returns:
            self
        """
        bits = 4096
        ran = run(['openssl', 'genrsa', str(bits)], stdout=PIPE, check=True)
        self.private = ran.stdout
        return self

    @property
    def private_key(self):
        """
        Provide the private key

        Returns:
            str: The private key
        """
        pkey = self.private.decode('utf-8')
        print(pkey)
        return self.private.decode('utf-8')

    @property
    def public_key(self):
        """
        Provision a pubkey if there isn't one already.

        Returns:
            str: the public key
        """
        if not self.public:
            private_key = serialization.load_pem_private_key(
                self.private,
                password=None,
                backend=default_backend()
            )
            self.public = private_key.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.PKCS1
            )

        return self.public

    def public_encrypt(self, data):
        """
        Encrypt something with the public key.

        Args:
            data (str): The thing to encrypt.

        Returns:
            str: The ciphertext
        """
        private_key = serialization.load_pem_private_key(
            self.private,
            password=None,
            backend=default_backend()
        )
        return private_key.public_key().encrypt(
            data, padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )


class RSAKeyAllOpenSSL:
    def __init__(self, data_path):
        """
        Do RSA key stuff with only system calls to openssl. :(

        Args:
            data_path (Path): path to the directory to store temporary data.
        """
        self.private = None
        self.public = None
        self.data_path = data_path

    def generate(self):
        """
        Use openssl to generate a public private key pair.

        Returns:
            self
        """
        bits = 2048
        ran = run([
            'openssl', 'genpkey', '-algorithm', 'RSA',
            '-pkeyopt', f'rsa_keygen_bits:{bits}'
        ], stdout=PIPE, check=True)
        self.private = ran.stdout
        return self

    @property
    def private_key(self):
        """
        Provide the private key

        Returns:
            str: The private key
        """
        pkey = self.private.decode('utf-8')
        print(pkey)
        return self.private.decode('utf-8')

    @property
    def public_key(self):
        """
        Provision a pubkey if there isn't one already.

        Returns:
            str: the public key
        """
        if not self.public:
            priv_key_path = self.data_path / 'tmp_priv_key'
            if not self.public:
                with open(priv_key_path, 'wb') as priv_file:
                    priv_file.write(self.private)
            thing = run(
                shlex.split(
                    f'openssl rsa -in {priv_key_path} -pubout'
                ),
                stdout=PIPE,
                check=True
            )
            self.public = thing.stdout
        return self.public

    def pub_key_file(self):
        """
        Write the public key to a file.

        Returns:
            str: The path to the file that was made.
        """
        pub_key_path = self.data_path / 'tmp_pub_key'
        with open(pub_key_path, 'wb') as pub_file:
            pub_file.write(self.public_key)

        return pub_key_path

    def public_encrypt(self, data):
        """
        Encrypt something with the public key.

        Args:
            data (str): The thing to encrypt.

        Returns:
            str: The ciphertext
        """
        pub_key_path = self.pub_key_file()
        clear_data_path = self.data_path / 'tmp_clr_dat'
        with open(clear_data_path, 'wb') as clr_file:
            clr_file.write(data)
        thing = run(
            shlex.split(
                'openssl rsautl -encrypt -pubin'
                f' -in {clear_data_path} -inkey {pub_key_path}'
            ),
            stdout=PIPE,
            check=True
        )
        return thing.stdout
