from pathlib import Path
import os

from black_mesa.environment import Environment
from black_mesa.remote_controller import RemoteController
from black_mesa.logger import Logger


class AssetsManager(object):
    def __init__(self):
        self.log = Logger(__name__).logger
        self.env = self.load_config()

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'assets': {
                'akmbr_archives': {
                    'ip': 'jupiter',
                    'archive_dir': '<path to the archive store>',
                    'username': '<user name to access the repository>',
                    'password': '<password to access the repository>'
                }
            }
        })
        return env.load_config()

    def download_akmbr(self, dir_name):
        """
        Pull a akmbr pair from assets archive to local test system.

        Args:
            dir_name (Path): Name of archive pair to pull

        Returns:
            Path: The local path to the resource directory.
        """
        akmbr_env = self.env['assets']['akmbr_archives']
        asset_dir = Path(akmbr_env['archive_dir'], dir_name)
        wait_secs = 15

        fake_app = 'akm_application_backup_00000000_000000'
        fake_sec = 'akm_secret_backup_00000000_000000'

        self.log.info(
            "Attempting to connect to jupiter. "
            f"Timeout set to {wait_secs} seconds..."
        )
        remote = RemoteController(
            akmbr_env['ip'], akmbr_env['username'],
            password=akmbr_env['password'], timeout=wait_secs
        )
        dest_dir = Path(self.env['workbench'], dir_name)
        if not dest_dir.is_dir():
            os.mkdir(str(dest_dir))
        remote.download(asset_dir / 'app', dest_dir / fake_app)
        dest = remote.download(asset_dir / 'sec', dest_dir / fake_sec)
        return dest.parents[0]
