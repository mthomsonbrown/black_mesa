from pathlib import Path
import shutil
from typing import Dict, List, Tuple
import pytest

import yaml

from black_mesa.terraform_controller import TerraformController
from black_mesa.remote_controller import RemoteController
from black_mesa.k8s_controller import K8SController
from black_mesa.environment import Environment
from black_mesa.fixtures.core import Core
from black_mesa.distribution_manager import DistributionManager


Cluster = Tuple[Path, Dict[Path, List[RemoteController]], str]


class K8S(Core):

    """
    Fixtures for k8s tests.
    """

    def copy_inf_scripts(self, workdir: Path):
        """
        Copy the terraform and ansible scripts to a temp directory.

        Args:
            workdir (Path): The directory to copy to
        """
        inf_dir = Path(__file__).parents[1] / 'infrastructure'

        shutil.copytree(inf_dir, workdir)

    def create_remote_sessions(self,
                               nodes: Dict[str, List[str]],
                               key_path: Path,
                               user: str) -> Dict[str, List[RemoteController]]:
        """
        Create remote sessions for all of the nodes in the cluster.

        Args:
            nodes: node role (master et al.) with list of ips in that role
            key_path: Filepath to the ssh key for the nodes
            user: Username for the nodes

        Returns:
            A dictionary of remote objects
        """
        remotes = {}
        for role in nodes:
            remotes[role] = []
            for sys in nodes[role]:
                remotes[role].append(RemoteController(
                    sys, user, key_path
                ))

        return remotes

    def update_volume_id(self, vol_id: int, conf_path: Path):
        """
        Change the volume id in an aws volume description file.

        Args:
            vol_id: The AWS ID for a EBS volume.
            conf_path: The path to the conf file to update.
        """
        with open(conf_path) as file_:
            vol_dat = yaml.load(file_, yaml.FullLoader)

        vol_dat['spec']['awsElasticBlockStore']['volumeID'] = vol_id

        with open(conf_path, 'w') as file_:
            yaml.dump(vol_dat, file_)

    def update_akm_document(self, vol_id: int, conf_path: Path):
        """
        Perform necessary updates to a multi-doc akm yaml description.

        Args:
            vol_id: The AWS ID for a EBS volume.
            conf_path: The path to the conf file to update.
        """
        with open(conf_path) as file_:
            dat = list(yaml.load_all(file_, yaml.FullLoader))

        for doc in dat:
            if doc['kind'] != 'PersistentVolume':
                continue
            doc['spec']['awsElasticBlockStore']['volumeID'] = vol_id

        with open(conf_path, 'w') as file_:
            yaml.dump_all(dat, file_)

    @pytest.fixture(scope='function')
    def workdir(self, worker_id: str) -> Path:
        """
        Form a path to be used for the current test run.

        Returns:
            Path: The path
        """
        env = Environment().load_config()
        workdir = Path(env['workbench']) / 'cluster_muck'
        workdir = workdir / f'thing_{worker_id}'
        return workdir

    @pytest.fixture(scope='function')
    def cluster(self, workdir: Path) -> Tuple[Path, Dict[str, List[str]]]:
        """
        Use terraform and ansible to spin up a kubernetes cluster in aws.

        Yields:
            I don't know quite yet...
            At least information for the kubernetes client library...
        """
        debug = False

        terra = TerraformController(workdir / 'terraform')

        if debug:
            terra.destroy()

        self.rmdir(workdir)
        self.copy_inf_scripts(workdir)

        if not debug:
            terra.init()

        terra.apply()

        key_path, user = terra.get_node_creds()
        nodes = terra.get_node_ips()

        cluster = {'workdir': workdir, 'key_path': key_path, 'user': user}
        cluster.update(nodes)

        remotes = self.create_remote_sessions(nodes, key_path, user)
        ebs_id = terra.get_ebs_id()
        try:
            yield workdir, remotes, ebs_id
        finally:
            if not debug:
                terra.destroy()

    @pytest.fixture(scope='function')
    def provisioned_cluster(self, cluster: Cluster):
        """
        Apply yaml files to cluster, install necessary test packages.
        """

        workdir, remotes, volume_id = cluster
        master = remotes['masters'][0]

        akm_yaml_path = Path(__file__).parents[1] / 'infrastructure' / 'k8s' / 'akm.yaml'

        # copy template file to temp directory
        shutil.copy(akm_yaml_path, workdir)

        # modify template files
        self.update_akm_document(volume_id, workdir / akm_yaml_path.name)

        # copy template file to cluster
        up_dir = Path('/home/ubuntu/')
        master.upload(workdir / akm_yaml_path.name, up_dir / akm_yaml_path.name)

        # Apply yaml files
        master.exec_check(f'kubectl apply -f {up_dir / akm_yaml_path.name}')

        # Do some akm key things
        k8s = K8SController(remotes['masters'], remotes['workers'])

        return k8s

    @pytest.fixture(scope='function')
    def asa_cluster(self, provisioned_cluster: K8SController):
        """
        Spin up a cluster, installing asa, for use with asa tests.
        """
        binaries = DistributionManager().get_binaries()

        # binaries = []

        for binary in binaries:
            provisioned_cluster.install_local_package(Path(binary))

        return provisioned_cluster
