import os
from contextlib import contextmanager

import pytest
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from akm_sdk.akm import Padding

from black_mesa.akm_controller import AKMController

from black_mesa.fixtures.core import Core


class AKMDFixtures(Core):
    plain_text = b"a secret message"
    aes_key_name = "aes_key"
    wrapper_key_name = 'private_key'

    @pytest.fixture(
        scope='function',
        params=[
            Padding.NONE,
            Padding.PKCS1,
            Padding.OAEP
        ]
    )
    def pad_mode(self, request):
        return request.param

    @pytest.fixture(
        scope='function',
        params=[i for i in range(10)]
    )
    def count(self, request):
        return request.param

    @pytest.fixture(scope='function')
    def akmd_akm(self):
        """
        Instantiate akm controller object for
        preprovisioned and initialized akm.
        Turn on akmd.
        Clear the database.
        """
        primary_ip = '192.168.73.211'
        password = 'OOHXPq6r530N6re'

        akm = AKMController(primary_ip, password=password)
        akm.turn_on_akmd()
        akm.python_sdk_controller.akm_admin.clean_db()
        akm.python_sdk_controller.akm_admin.set_log_level(50)
        return akm

    @pytest.fixture(scope='function')
    def rand_aes_key(self):
        return os.urandom(32)

    def do_encrypt_aes_ecb(self, key):
        """
        Encrypt some test data.

        Args:
            key (bytes): The AES key to use to encrypt

        Returns:
            bytes: The encrypted data
        """
        cipher = Cipher(
            algorithms.AES(key),
            modes.ECB(),
            backend=default_backend()
        )

        encryptor = cipher.encryptor()
        return encryptor.update(self.plain_text) + encryptor.finalize()

    def do_encrypt_aes_cbc(self, key, iv):
        """
        Encrypt some test data.

        Args:
            key (bytes): The AES key to use to encrypt
            iv (bytes): The initialization vector to use

        Returns:
            bytes: The encrypted data
        """
        cipher = Cipher(
            algorithms.AES(key),
            modes.CBC(iv),
            backend=default_backend()
        )

        encryptor = cipher.encryptor()
        return encryptor.update(self.plain_text) + encryptor.finalize()

    def do_local_decrypt_aes_ecb(self, user, cipher_text):
        """
        Retrieve an aes key from the akm and use it to decrypt a thing
        using ECB mode. Assert the output matches the plain text value.

        Args:
            user (AKMClient): Python SDK client controller
            cipher_text (bytes): The previously encrypted text data
        """
        # download aes key
        resp = user.get_aes_key(self.aes_key_name)

        # decrypt with aes key
        cipher = Cipher(
            algorithms.AES(resp['KeyValue']),
            modes.ECB(),
            backend=default_backend()
        )
        decryptor = cipher.decryptor()
        decrypted = decryptor.update(cipher_text) + decryptor.finalize()
        assert decrypted == self.plain_text

    def decrypt_aes_cbc(self, cipher_text, key, iv):
        """
        Decrypt a thing using CBC mode. Assert the output matches the
        plain text value.

        Args:
            cipher_text (bytes): The previously encrypted text data.
            key (bytes): The key to decrypt with.
            iv (bytes): The initialization vector to use
        """
        cipher = Cipher(
            algorithms.AES(key),
            modes.CBC(iv),
            backend=default_backend()
        )
        decryptor = cipher.decryptor()
        decrypted = decryptor.update(cipher_text) + decryptor.finalize()
        assert decrypted == self.plain_text

    def do_local_decrypt_aes_cbc(self, user, cipher_text, iv):
        """
        Retrieve an aes key from the akm and use it to decrypt a thing
        using CBC mode. Assert the output matches the plain text value.

        Args:
            user (AKMClient): Python SDK client controller
            cipher_text (bytes): The previously encrypted text data
            iv (bytes): The initialization vector to use
        """
        resp = user.get_aes_key(self.aes_key_name)
        self.decrypt_aes_cbc(cipher_text, resp['KeyValue'], iv)

    @contextmanager
    def pcidss_off(self, akm):
        """
        Turn PCIDSS mode off in the config file for a given context.
        Set the PCIDSS mode back to what it was before the context.

        Args:
            akm (AkmController): The controller for the akm you want
                to change.
        """
        new_mode = 'N'

        try:
            orig_mode = akm.get_pcidss_mode()
            if orig_mode != new_mode:
                akm.set_pcidss_mode(new_mode)
            akm.python_sdk_controller.akm_admin.set_log_level(50)
            yield
        finally:
            akm.set_pcidss_mode(orig_mode)

    @contextmanager
    def pcidss_on(self, akm):
        """
        Turn PCIDSS mode on in the config file for a given context.
        Set the PCIDSS mode back to what it was before the context.

        Args:
            akm (AkmController): The controller for the akm you want
                to change.
        """
        new_mode = 'Y'

        try:
            orig_mode = akm.get_pcidss_mode()
            if orig_mode != new_mode:
                akm.set_pcidss_mode(new_mode)
            akm.python_sdk_controller.akm_admin.set_log_level(50)
            yield
        finally:
            if orig_mode != new_mode:
                akm.set_pcidss_mode(orig_mode)
