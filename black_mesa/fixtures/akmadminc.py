from contextlib import ExitStack
from contextlib import contextmanager

import pytest

from black_mesa.python_sdk_controller import PythonSDKController
from black_mesa.akmadminc_controller import AKMAdminCController
from black_mesa.akm_controller import AKMController
from black_mesa.logger import Logger
from black_mesa.aws_controller import AWSController

from black_mesa.fixtures.core import Core


class AKMAdminC(Core):

    log = Logger(__name__).logger

    @pytest.fixture(scope='class')
    def clients(self):
        """
        Create a new AKM instance in AWS, initialize the AKM, pull PKI onto
        the test system, and initialize both akmadminc and akm_libraries_py
        interfaces.

        Returns:
            PythonSDKClient: Interface to akm_libraries_py
            AKMAdminC: Interface to akmadmin.c
        """
        with self.aws_system() as aws_sys:
            akm = AKMController(aws_sys['ip'],
                                key=aws_sys['key'])

            ip = '192.168.73.178'
            password = 'OOHXPq6r530N6re'
            akm = AKMController(ip, password=password)

            # akm.initialize('drogon')
            psc = PythonSDKController(
                akm.ip, akm.username, key=akm.key, password=akm.password
            )
            psc.download_pki()
            psc.compile_xml()
            aac = AKMAdminCController(
                akm.ip, akm.username, key=akm.key, password=akm.password
            )
            # aac.install_akmadminc()
            # aac.install_akmadminc_from_source(Path('/home/mike/source_code/akmadmin'))
            aac.download_pki()
            aac.upload_pki(aac.local_storage)
            aac.compile_conf()
            aac.upload_conf()
            try:
                yield psc, aac
            finally:
                akm.cleanup()

    @contextmanager
    def system(self, instance_id=None, ip=None, password=None, key=None):
        aws = AWSController()
        creds = {}

        if instance_id:
            if ip or password or key:
                raise ValueError(
                    "When requesting system by instance_id, "
                    "only instance_id should be provided."
                )
            (creds['ip'],
             creds['key']) = aws.connect_to_instance(instance_id)
            creds['password'] = None
        elif ip:
            if not password and not key:
                raise ValueError(
                    "When requesting a system by ip, a password or key "
                    "must be provided."
                )
            creds['ip'] = ip
            creds['key'] = key
            creds['password'] = password
        else:
            (creds['ip'],
             creds['key']) = aws.create_instance()
            creds['password'] = None
        try:
            yield creds
        finally:
            if not ip:
                pass
                # aws.terminate_instances()

    @pytest.fixture(scope='class')
    def mirror_systems(self):
        """
        Create two new AKM instances in AWS, initialize them, pull PKI onto
        the test system, and initialize both akmadminc and akm_libraries_py
        interfaces.

        Returns:
            PythonSDKClient: Interface to akm_libraries_py
            AKMAdminC: Interface to akmadmin.c
        """
        p_instance = None
        p_ip = None
        p_ip = '192.168.73.232'
        p_key = None
        p_password = None
        p_password = 'OOHXPq6r530N6re'
        s_instance = None
        s_ip = None
        s_ip = '192.168.73.170'
        s_key = None
        s_password = None
        s_password = 'OOHXPq6r530N6re'

        primary_name = 'drogon'
        with ExitStack() as stack:
            prime_sys = stack.enter_context(
                self.system(p_instance, p_ip, p_password, p_key)
            )
            sec_sys = stack.enter_context(
                self.system(s_instance, s_ip, s_password, s_key)
            )
            prime_akm = AKMController(
                prime_sys['ip'],
                key=prime_sys['key'],
                password=prime_sys['password']
            )
            sec_akm = AKMController(
                sec_sys['ip'],
                key=sec_sys['key'],
                password=sec_sys['password']
            )
            # prime_akm.initialize(primary_name)
            # sec_akm.initialize(secondary_name)

            prime_akm.python_sdk_controller.akm_admin.clean_db()
            sec_akm.python_sdk_controller.akm_admin.clean_db()

            sec_akm.upload_file(prime_akm.download_ssh_pubkey())
            sec_akm.legacy_bootstrap_controller.set_as_mirror(primary_name)
            prime_akm.legacy_bootstrap_controller.add_mirror(sec_akm.ip)

            try:
                yield prime_akm, sec_akm
                # yield None, None, None, None
            finally:
                prime_akm.cleanup()
                sec_akm.cleanup()
