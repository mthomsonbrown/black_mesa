"""core.py"""
import time
import shutil
from contextlib import contextmanager
from pathlib import Path

import pytest

from black_mesa.aws_controller import AWSController
from black_mesa.akm_controller import AKMController
from black_mesa.azure_controller import AzureController
from black_mesa.environment import Environment
from black_mesa.logger import Logger
from black_mesa.distribution_manager import DistributionManager

log = Logger(__name__).logger


class Core:
    """Base test functionality to be inherited by test suites."""
    log_dir = Path('/var/log/townsend/')
    bootstrap_log_path = log_dir / 'bootstrap.log'
    audit_log_path = log_dir / 'akmaudit.log'
    err_log_path = log_dir / 'akmerror.log'
    trace_log_path = log_dir / 'akmtrace.log'
    auth_key_log_path = log_dir / 'create-auth-key.log'
    kek_log_path = log_dir / 'create-kek.log'
    logrotate_file = Path('/etc/logrotate.d/akm')

    @pytest.fixture(
        scope='function',
        params=[
            err_log_path,
            audit_log_path,
            trace_log_path,
            bootstrap_log_path
        ]
    )
    def log_path(self, request):
        """Iterate over log files affected by logrotate."""
        return request.param

    @pytest.fixture(
        scope='function',
        params=[
            err_log_path,
            audit_log_path,
            trace_log_path,
            pytest.param(
                bootstrap_log_path,
                marks=pytest.mark.xfail(reason='BOOT-77', strict=True)
            ),
        ]
    )
    def log_path_bs_fail(self, request):
        """
        Iterate over log files affected by logrotate.
        Marks bootstrap tests with strict xfail.  Can remove this function
        after BOOT-77 is addressed and replace with `log_path`.
        """
        return request.param

    @contextmanager
    def aws_system(self, instance_id=None, kill=True):
        """
        Provision an AWS EC2 instance.

        Args:
            instance_id: If used, return handle to system specified.
                Default will create a new instance.
            kill (bool): If True, terminate instance after leaving scope.

        Yields:
            dict: The IP and key for the aws system.
        """
        aws = AWSController()
        creds = {}

        if instance_id:
            (creds['ip'],
             creds['key']) = aws.connect_to_instance(instance_id)
        else:
            (creds['ip'],
             creds['key']) = aws.create_instance()
        try:
            yield creds
        finally:
            if kill:
                aws.terminate_instances()

    def aws_sys_man(self, instance_id=None):
        """
        Provision an AWS EC2 instance.

        Args:
            instance_id: If used, return handle to system specified.
                Default will create a new instance.

        Returns:
            str: The IP of the system.
            str: The key used to log into the system.
        """
        aws = AWSController()
        creds = {}

        if instance_id:
            (creds['ip'],
             creds['key']) = aws.connect_to_instance(instance_id)
        else:
            (creds['ip'],
             creds['key']) = aws.create_instance()
        return creds

    @contextmanager
    def azure_system(self):
        """
        Provision an AWS EC2 instance.

        Yields:
            str: The IP of the system.
            str: The key used to log into the system.
        """
        azure = AzureController()
        ip_address = azure.create_instance(1)

        try:
            yield ip_address
        finally:
            # TODO destroy instance
            pass

    @pytest.fixture(scope='function')
    def fresh_akm(self):
        """
        Provision an AKM on a fresh AWS instance.

        Yields:
            AkmController: Handle to the new AKM.
        """
        # instance = 'i-0804becede4ee76ff'
        instance = None

        with self.aws_system(instance, kill=True) as aws_sys:
            akm = AKMController(aws_sys['ip'], key=aws_sys['key'])

            try:
                log.info("AKM IP: %s", akm.ip)
                yield akm
            finally:
                akm.cleanup()

    @pytest.fixture()
    def legacy_bs_akm(self):
        """
        Use the legacy bootstrap cli methods to deploy local bootstrap 2.x
        files to an akm.

        Yields:
            AkmController: handle to the provisioned akm.
        """
        env = Environment().load_config()

        with self.aws_system(kill=False) as aws_sys:
            akm = AKMController(aws_sys['ip'],
                                key=aws_sys['key'])

            akm.install_bootstrap_api(
                Path(env['workbench']) / 'api_surrogate' / 'bootstrap' /
                'bootstrap' / 'api'
            )

            akm.upload_file(Path('/home/mike/source_code/bootstrap/bootstrap'))

            akm.legacy_bootstrap_controller.deploy_bootstrap()

            try:
                log.info("AKM IP: %s", akm.ip)
                yield akm
            finally:
                akm.cleanup()

    @pytest.fixture
    def azure(self):
        with self.azure_system() as az_sys:
            try:
                yield az_sys
            finally:
                pass

    @pytest.fixture(scope='function')
    def bs_akm(self, fresh_akm):
        """
        Build bootstrap 3.x and install on fresh aws akm.

        Yields:
            AkmController: handle to the provisioned akm.
        """
        binaries = []

        binaries = DistributionManager().get_binaries()

        fresh_akm.wait_for_dpkg_lock()
        fresh_akm.remote.exec_check('sudo apt-get update')
        for binary in binaries:
            fresh_akm.apt_install_package(Path(binary))
        try:
            yield fresh_akm
        finally:
            fresh_akm.cleanup()

    @pytest.fixture(scope='class')
    def aws_akm_conn(self):
        """
        Provide a connection to a pre-deployed aws akm.

        Yields:
            AkmController: handle to the akm.
        """
        instance_id = 'i-0308d48a867ff4881'
        with self.aws_system(instance_id) as aws_sys:
            akm = AKMController(aws_sys['ip'], key=aws_sys['key'])
        try:
            yield akm
        finally:
            pass

    @pytest.fixture(scope='function')
    def worker_id(self, request) -> str:
        """
        If tests are run in parallel, determine
        the current test's identifier.

        Returns:
            The test ID if running in parallel, else 'master'
        """
        worker_id = 'master'
        if hasattr(request.config, 'slaveinput'):
            worker_id = request.config.slaveinput['slaveid']
        log.debug(f'Worker ID: {worker_id}')
        return worker_id

    def rmdir(self, target_dir: Path):
        """
        Remove a directory and its contents if it exists.

        Args:
            target_dir (Path): The specified local directory.
        """
        if target_dir.exists():
            shutil.rmtree(target_dir)

    def psc_wait(self, command, err, *args, state='0000', timeout=5, **kwargs):
        """
        Send a request using the python sdk.  Wait for the expected state
        from the akm.

        Args:
            command (method): The method to call on the client.
            err (method): The method to call to get the error code.
            *args: positional arguments to pass to the client.
            return code (str): The expected value returned by akmd.
            timeout (int): How many seconds to wait for the desired state.
            **kwargs: keyword arguments to pass to the client.
        """
        polling_frequency = 0.5
        time_stop = time.time() + timeout
        while time.time() < time_stop:
            command(*args, **kwargs)
            if err() == state:
                return
            time.sleep(polling_frequency)
        assert err() == state, err()
