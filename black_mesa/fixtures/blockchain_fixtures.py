import os
import multiprocessing as mp
from collections import deque
import glob

import pytest

from black_mesa.fixtures.core import Core

from black_mesa.azure_controller import AzureController
from black_mesa.akm_controller import AKMController
from black_mesa.environment import Environment


class BlockchainFixtures(Core):
    def prep_blockchain(self, akm):
        """Perform necessary updates to blockchain application on AKM."""
        env = Environment().load_config()
        akm.download_blockchain(os.path.join(env['workbench'], 'blockchain'))
        akm.blockchain.install_node()
        akm.blockchain.install_dependencies()
        akm.blockchain.open_firewall()
        akm.blockchain.configure_akm()
        akm.blockchain.run_prerequisites()
        akm.blockchain.fix_permissions()

    def prep_bootstrap(self, akm):
        """Perform necessary updates to bootstrap application on AKM."""
        env = Environment().load_config()
        akm.install_bootstrap_api(
            os.path.join(env['workbench'],
                         'bootstrap', 'bootstrap', 'api'))

        for thing in glob.glob(
                os.path.join(env['workbench'], 'akm_*')):
            akm.upload_file(thing)

        akm.install_akmdb_tool(
            os.path.join(env['workbench'], 'akmdb_tool')
        )

        akm.upload_file(
            os.path.join(env['workbench'], 'bootstrap', 'bootstrap'))

        akm.bootstrap.deploy_bootstrap()

    def launch_network(self, akms):
        assert len(akms) >= 3, "At least three akms required to launch network"

        ip_0 = akms[0].ip
        ip_1 = akms[1].ip
        ip_2 = akms[2].ip

        akms[1].blockchain.exec_check_bc_repo(
            "yes | NODE_ENV=stage node manager.js --name=member "
            "--ip='{ip_0}, {ip_1}, {ip_2}' --this=1 "
            "--dms --storage --install >/dev/null".format(
                ip_0=ip_0, ip_1=ip_1, ip_2=ip_2
            ))
        akms[2].blockchain.exec_check_bc_repo(
            "yes | NODE_ENV=stage node manager.js --name=member "
            "--ip='{ip_0}, {ip_1}, {ip_2}' --this=2 "
            "--dms --storage --install >/dev/null".format(
                ip_0=ip_0, ip_1=ip_1, ip_2=ip_2
            ))

    @pytest.fixture
    # pylama:ignore=C901
    # The whole blockchain system should be rewritten, probably to use terraform/ansible.
    def nodes(self):
        """
        Provision three akm blockchain nodes.

        Returns:
            IP Addresses for the nodes
        """
        num_nodes = 3
        throttle = num_nodes  # set for max number concurrent processes
        azure = AzureController()
        ip_queue = mp.Queue()
        akm_ips = mp.Queue()
        akms = []
        # reuse_ips = []
        #                  0                 1                2
        reuse_ips = ['104.42.249.156', '104.42.250.10', '104.42.254.90']

        def spawn_instance(instance_id):
            ip_queue.put(azure.create_instance(instance_id))

        def prepare_akm(ip):
            akm = AKMController(ip)
            self.prep_bootstrap(akm)
            self.prep_blockchain(akm)
            akm_ips.put(akm.ip)

        azure_jobs = [
            mp.Process(target=spawn_instance, args=(id,))
            for id in range(num_nodes)
        ]

        akm_jobs = []

        running = deque([])
        if not reuse_ips:
            for job in azure_jobs:
                running.append(job)

                while len(running) > throttle:
                    task = running.popleft()
                    task.join()
                    akm_job = mp.Process(
                        target=prepare_akm, args=(ip_queue.get(),))
                    akm_jobs.append(akm_job)
                    akm_job.start()

                job.start()
            while running:
                task = running.popleft()
                task.join()
                akm_job = mp.Process(
                    target=prepare_akm, args=(ip_queue.get(),))
                akm_jobs.append(akm_job)
                akm_job.start()

            for job in akm_jobs:
                job.join()
        else:
            for ip in reuse_ips:
                akm_job = mp.Process(
                    target=prepare_akm, args=(ip,))
                akm_jobs.append(akm_job)
                akm_job.start()
            for job in akm_jobs:
                job.join()

        while not akm_ips.empty():
            akms.append(AKMController(akm_ips.get()))

        # self.launch_network(akms)

        try:
            yield akms
        finally:
            pass  # TODO: Clean up AKMs
