import os
import sys
import argparse
from subprocess import run
from pathlib import Path

from black_mesa.build_manager import (
    BootstrapBuilder,
    BrapiBuilder,
    AKM2JSONBuilder,
    PlankBuilder,
    ASABuilder,
    EKMAdminPackager
)

from black_mesa.akm_controller import AKMController


def get_args():
    parser = argparse.ArgumentParser(
        description="Working to make a better tomorrow for all mankind."
    )
    subparser = parser.add_subparsers(dest='command')

    add_build_parser(subparser)
    add_package_parser(subparser)
    add_test_parser(subparser)
    add_akm_parser(subparser)

    return parser.parse_args()


def add_build_parser(subparser):
    """
    Add build commands to subparser
    """
    build = subparser.add_parser(
        'build',
        help='Build a project.'
    )
    build.add_argument(
        'project',
        help='Name of project to build'
    )
    build.add_argument(
        '--source', '-s',
        help='The local path to the source directory.',
        dest='src_path'
    )
    build.add_argument(
        '--branch', '-b',
        help='The name of the branch to use for the build.',
        dest='branch_name'
    )
    build.add_argument(
        '--export', '-e',
        help=(
            'The directory to copy the package to. '
            'If left blank, the local workspace will be used.'),
        dest='export_path'
    )
    build.add_argument(
        '--hostname', '-hn',
        help=(
            'The hostname or ip of the system to copy to. If left blank, '
            'will be set to localhost.'),
        dest='hostname'
    )
    build.add_argument(
        '--username', '-u',
        help=('The username for the remote system.'),
        dest='username'
    )
    build.add_argument(
        '--key', '-k',
        help='If using an ssh key, the local path to that key.',
        dest='key'
    )
    build.add_argument(
        '--password', '-p',
        help='The password, if using, to authenticate to the remote repository.',
        dest='password'
    )
    build.add_argument(
        '--overwrite', '-o',
        help='Overwrite the output file if it already exists',
        action='store_true'
    )
    build.add_argument(
        '--outfile', '-O',
        help=(
            'Path to create a file containing the absolute path '
            'of the created binary.'
        ),
        dest='out'
    )
    build.add_argument(
        '--config', '-c',
        help=('Path to the black_mesa.json file'),
        dest='config'
    )


def do_build(args):
    build_project = {
        'bootstrap': build_bootstrap,
        'brapi': build_brapi,
        'akm2json': build_akm2json,
        'plank': build_plank,
        'asa': build_asa,
    }
    if args.project not in build_project:
        print("Unknown project: {}".format(args.project))
        sys.exit(1)
    build_project[args.project](args)
    return


def build_bootstrap(args):
    build_a_thing(args, BootstrapBuilder)


def build_brapi(args):
    build_a_thing(args, BrapiBuilder)


def build_akm2json(args):
    build_a_thing(args, AKM2JSONBuilder)


def build_plank(args):
    build_a_thing(args, PlankBuilder)


def build_asa(args):
    build_a_thing(args, ASABuilder)


def build_a_thing(args, builder):
    if args.config:
        os.environ['black_mesa_config'] = args.config
    path = builder(
        src_path=args.src_path,
        branch_name=args.branch_name,
        export_path=args.export_path,
        hostname=args.hostname,
        username=args.username,
        key=args.key,
        password=args.password,
        overwrite=args.overwrite
    ).build()
    print("Created package: {}".format(path))
    if not args.out:
        return
    with open(args.out, 'w') as outfile:
        outfile.write(str(path))


def add_package_parser(subparser):
    """
    Add packaging commands to subparser.
    """
    pkg = subparser.add_parser(
        'pkg',
        help='Create a package from a set of wheels.'
    )
    pkg.add_argument(
        'project',
        help='The name of the package to build.'
    )
    pkg.add_argument(
        '--config', '-c',
        help=('Path to the black_mesa.json file'),
        dest='config',
        required=True
    )


def do_pkg(args):
    """
    Package the requested thing.
    """
    project = {
        'ekm_admin': package_ekm_admin
    }
    if args.project not in project:
        print("Unknown project: {}".format(args.project))
        sys.exit(1)
    project[args.project](args)
    return


def package_ekm_admin(args):
    """
    Package ekm_admin.
    """
    os.environ['black_mesa_config'] = args.config
    EKMAdminPackager().package()


def add_test_parser(subparser):
    """
    Add test commands to subparser
    """
    test = subparser.add_parser(
        'test',
        help='Run some functional tests.'
    )
    test.add_argument(
        '--mark', '-m',
        help=('Keyword to pass to pytest to select which tests to run.'),
        dest='mark'
    )
    test.add_argument(
        '--config', '-c',
        help=('Path to the black_mesa.json file'),
        dest='config'
    )
    test.add_argument(
        '--junit', '-j',
        help=('Path to output the junit results to.'),
        dest='junit'
    )
    test.add_argument(
        '--num', '-n',
        help='Number of parallel tests to run.',
        dest='num'
    )


def do_test(args):
    if args.config:
        os.environ['black_mesa_config'] = str(Path(args.config).resolve())
    cmd = 'python3 -m pytest'
    if args.mark:
        cmd = f'{cmd} -m {args.mark}'
    if args.junit:
        cmd = f'{cmd} --junitxml={args.junit}'
    if args.num:
        cmd = f'{cmd} -n {args.num}'
    run(cmd, cwd=Path(__file__).parent, shell=True, check=True)


def add_akm_parser(subparser):
    """
    Add commands to invoke akm api through python sdk
    """
    akm = subparser.add_parser(
        'akm',
        help='Provision PKI and invoke akm commands.'
    )
    akm.add_argument(
        '--ip',
        help='The IP of the AKM to run the command on.',
        dest='ip',
        required=True
    )
    akm.add_argument(
        '--key', '-k',
        help='The ssh key for the akm, if using a key.',
        dest='key'
    )
    akm.add_argument(
        '--password', '-p',
        help='The password for the akm, if using a password.',
        dest='password'
    )


def do_akm(args):
    """
    Perform an akm opperation.

    NOTE:
        This should be generalized. For now it just does a
        set opperation as a sanity check.
    """
    akm = AKMController(args.ip, key=args.key, password=args.password)
    akm_con = akm.python_sdk_controller
    akm_con.akm_admin.create_aes_key('test_key')
    akm_con.akm_client.get_aes_key('test_key')


def main():
    args = get_args()
    cmd = {
        'build': do_build,
        'pkg': do_pkg,
        'test': do_test,
        'akm': do_akm
    }
    if args.command not in cmd:
        print("Unknown command: {}".format(args.command))
        return
    cmd[args.command](args)


if __name__ == '__main__':
    main()
