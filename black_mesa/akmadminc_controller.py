import time
import re
import subprocess
from pathlib import Path
from collections import defaultdict

from black_mesa.logger import Logger
from black_mesa.environment import Environment
from black_mesa.client_controller import ClientController


class AKMAdminCController(ClientController):
    """
    Download PKI from an AKM and create a conf file for akmadmin.c.
    """
    def __init__(self, ip, username, key=None, password=None):
        super().__init__(ip, username, key=key, password=password)
        self.log = Logger(__name__).logger
        self.env = self.load_config()
        self.workbench = self.env['workbench']
        self.env = self.env['akmadminc']

        self.sys_ip = ip
        self.username = username
        self.key = key

        self.remote_cert = None
        self.remote_key = None
        self.remote_ca = None
        self.conf_loc = Path(self.workbench, 'akmadmin.conf')

    def load_config(self):
        env = Environment()
        home_dir = '/home/admin'
        env.config_json.update({
            'akmadminc': {
                'akmadminc_dir': str(Path(home_dir, '.akmadmin')),
                'upload_dir': str(Path(home_dir, 'uploads'))
            }
        })
        return env.load_config()

    def install_akmadminc_from_source(self, path):
        """
        Install akmadminc from source.

        Args:
            path (Path): Path to local akmadminc source
        """
        subprocess.check_call('make', cwd=str(path))
        self.remote.upload(path / 'akmadmin', Path('/home/admin/akmadmin'))
        self.remote.exec_check('sudo dpkg --remove akmadmin', ignore='ignoring')
        self.remote.exec_check('sudo chmod 777 /home/admin/akmadmin')

    def wait_for_dpkg_lock(self, timeout):
        """
        Check the dpkg lock file.  Return when free, or raise exception
        if timeout reached.

        Args:
            timeout (float): Seconds to wait before giving up.
        """
        polling_frequency = 0.5
        time_stop = time.time() + timeout
        while time.time() < time_stop:
            _, stdout, _ = self.remote.ssh.exec_command(
                'sudo lsof /var/lib/dpkg/lock'
            )
            out = stdout.readlines()
            if not out:
                return
            time.sleep(polling_frequency)
        raise IOError("Timeout limit reached.  dpkg lock not released.")

    def upload_pki(self, local_pki):
        """
        Push target AKM's PKI to the akmadminc client system.

        This pushes the first instance of admin PKI found in the directory, and
        compiles paths to those files on the remote system for use in compiling
        a config file.

        Args:
            local_pki (Path): Location of the AKM's client PKI.
        """
        self.log.info("Uploading PKI to akmadminc client.")
        for client in self.iter_clients(local_pki):
            cert, key, root_ca = self.find_cert_key_ca(client)
            if not self.is_admin(cert):
                continue
            upload_dir = Path(self.env['upload_dir'])
            self.remote_cert = self.remote.upload(cert, upload_dir)
            self.remote_key = self.remote.upload(key, upload_dir)
            self.remote_ca = self.remote.upload(root_ca, upload_dir)
            return

    def compile_conf(self):
        """Create the akmadmin.conf file."""
        with open(str(self.conf_loc), 'w') as conf_file:
            conf_file.write('\n'.join([
                '[IP]',
                'KeyServerIp=localhost',
                'KeyServerAdminPort=6001',
                'ConnectTimeoutSecs=5',
                'ConnectTimeoutMSecs=0',
                '[cert]',
                'TrustedCACertDir={}'.format(self.env['upload_dir']),
                'TrustedCACert={}'.format(self.remote_ca),
                'AdminPrivKey={}'.format(self.remote_key),
                'AdminSignedCert={}'.format(self.remote_cert),
                ''
            ]))

    def upload_conf(self):
        """Push conf file to remote system."""
        self.remote.upload(self.conf_loc, Path(self.env['akmadminc_dir']))

    def execute(self, cmd):
        """
        Invoke akmadminc.

        Args:
            cmd (str): The command to send to akmadminc.

        Returns:
            dict: Formatted output from akmadmin
        """
        res = {}
        cmd = 'akmadmin {}'.format(cmd)
        self.log.debug(f"Issuing command:\n{cmd}")
        _, stdout, stderr = self.remote.ssh.exec_command(cmd)
        out = stdout.readlines()
        err = stderr.readlines()

        outstr = '\n'.join(out)
        self.log.debug(f"Result:\n{outstr}")
        errstr = '\n'.join(err or ['None'])
        self.log.debug(f"Error:\n{errstr}")
        res['msg'] = self.str_res_to_dict(out)
        res['err'] = self.str_err_to_dict(err)
        return res

    def str_res_to_dict(self, result):
        """
        Parses akmadmin stdout into a dictionary of fields and values.

        Args:
            result (list): Success message output from akmadmin.

        Returns:
            dict: The formatted output.
        """
        if not result:
            return {}
        ret = defaultdict(list)
        for line in result:
            if line.startswith('**'):
                continue
            if '<' not in line:
                continue
            if line.startswith('Invalid number of options'):
                ret['Invalid number of options'] = {
                    'expected': re.split('<|>', line)[3],
                    'given': re.split('<|>', line)[1]
                }
                continue
            ret[line.split('<')[0].strip()].append(
                line.split('<')[1].strip('<>\n'))

        for key, value in ret.items():
            if len(value) == 1:
                ret[key] = value[0]

        return ret

    def str_err_to_dict(self, err):
        """
        Parses akmadmin stderr into a dictionary of lists of error codes
        and messages.

        Args:
            err (list): Error message output from akmadmin.

        Returns:
            dict: The formatted output.
        """
        if not err:
            return {}
        ret = defaultdict(list)
        for line in err:
            ret[line.split('<')[1].split('>')[0]].append(line)
        return ret
