import time
import socket
import json
from datetime import datetime

import paramiko

from black_mesa.logger import Logger
from black_mesa.environment import Environment


class RemoteController:
    def __init__(self, ip, username, key=None, password=None, timeout=None):
        self.log = Logger(__name__).logger
        self.env = self.load_config()['akm']

        self.sys_ip = ip
        self.username = username
        self.password = password
        self.key = key
        if not password and not key:
            raise ValueError("Either a key or password must be provided.")

        self.ssh = paramiko.SSHClient()

        self.connect_to_ssh(timeout)

        self.sftp = self.ssh.open_sftp()
        self._name = None
        self.log.debug("Connection Established: %s", ip)

    def __str__(self):
        """
        Default to printing the ip address of a system.
        """
        return self.sys_ip

    def __repr__(self):
        """
        For printing inside a list or dict.
        """
        return self.sys_ip

    @property
    def name(self):
        """
        Get hostname of AKM.

        Returns:
            str: The name.
        """
        _, out, _ = self.ssh.exec_command('hostname')
        self._name = out.readlines()[0].strip()
        return self._name

    def reset_connection(self):
        """
        Reacquire connections to ssh and sftp
        """
        self.ssh.close()
        self.connect_to_ssh()
        self.sftp = self.ssh.open_sftp()

    def load_config(self):
        env = Environment()
        return env.load_config()

    def connect_to_ssh(self, timeout=None):
        """Set up paramiko SSH connection."""
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        retry_attempts = 10
        wait_secs = 1
        for _ in range(retry_attempts):
            try:
                self.attempt_ssh_connection(timeout=timeout)
                return
            except (
                    socket.timeout,
                    paramiko.ssh_exception.NoValidConnectionsError
            ):
                self.log.info(
                    "SSH connection attempt failed.  "
                    f"Waiting {wait_secs} seconds and trying again."
                )
                time.sleep(wait_secs)
        self.attempt_ssh_connection(timeout=timeout)

    def attempt_ssh_connection(self, timeout=None):
        """
        Try to connect to ssh.  Paramiko might throw an exception if ssh
        isn't ready on the remote system yet, which can be an issue with AWS
        systems.
        """
        if self.key:
            paramiko_key = paramiko.RSAKey.from_private_key_file(self.key)
            self.ssh.connect(self.sys_ip, username=self.username,
                             pkey=paramiko_key, timeout=timeout)
        else:
            self.ssh.connect(self.sys_ip, username=self.username,
                             password=self.password, timeout=timeout)

    def list_dir(self, target):
        """
        Report contents of a directory.

        Args:
            target (str): Path on remote system of directory to list.

        Returns:
            (list) Paths to files inside target directory.
        """
        self.connect_to_ssh()
        ftp = self.ssh.open_sftp()
        return ftp.listdir(target)

    def download(self, source, dest):
        """
        Copy a file from the remote system to a local directory.

        Args:
            source (Path): The remote path to the file to download.
            dest (Path): Path to the local directory to download to.

        Returns:
            Path: The filepath of the downloaded file
        """
        ftp = self.ssh.open_sftp()
        if dest.is_dir():
            dest = dest / source.name
        ftp.get(str(source), str(dest))
        return dest

    def upload(self, source, destination):
        """
        Copy a file or directory from the local system to a remote directory.

        Args:
            source (Path): The local path to the file to upload.
            destination (Path): Path to the remote directory to upload to.

        Returns:
            Path: The location of the uploaded item on the remote system.
        """
        if source.is_file():
            if source.name != destination.name:
                destination = destination / source.name
            self.log.info(f"Uploading file: {source} to {destination}")
            self.ssh.exec_command('mkdir -p {}'.format(destination.parents[0]))
            self.sftp.put(str(source), str(destination))
            return destination

        self.ssh.exec_command('mkdir {}'.format(destination))
        self.log.info(f"Uploading directory: {source} to {destination}")
        self.do_upload(source, destination)
        return destination

    def do_upload(self, source, destination):
        """Recursively copy a folder structure onto the remote system."""
        for item in source.iterdir():
            target_dest = destination / item.relative_to(source)
            if item.is_file():
                self.sftp.put(str(item),
                              str(target_dest))
            else:
                self.ssh.exec_command('mkdir {}'.format(target_dest))
                self.do_upload(item, target_dest)

    def date(self):
        """
        Get the current date and time from the system.

        Returns:
            datetime: Object representation of the date and time.

        NOTE: Can't use epoch from the akm, because when you convert it
            locally to a datetime, it will use the local timezone.
        """
        akm_date = self.exec_check('date').decode('utf-8').strip()
        return datetime.strptime(akm_date, '%a %b  %d %H:%M:%S %Z %Y')

    def exec_check(self, cmd, ignore=None, verbose=False, json_out=False) -> bytes:
        """
        Execute a command on the AKM.  Throw an assertion error if the
        command returns an error.

        Args:
            cmd (str): The command to execute.
            ignore (str or list): If error body contains this text,
                don't raise the exception.
            verbose (bool): If True, log the command and output.
            json_out (bool): If True, convert the output to json

        Returns:
            bytes: The contents of stdout.
        """
        _, out, err = self.ssh.exec_command(cmd)
        if verbose:
            self.log.debug('Executing command: %s', cmd)
            self.log.debug('Command result: %s', out.readlines())
        err_body = err.readlines()
        if ignore:
            if not isinstance(ignore, list):
                ignore = [ignore]
            for string in err_body:
                for statement in ignore:
                    if statement in string:
                        return out.read()
        if err_body:
            raise OSError(
                "\nCommand: {}\nError: {}".format(cmd, ''.join(err_body))
            )
        if json_out:
            return json.loads(out.read().decode('utf-8'))
        return out.read()

    def async_exec(self, cmd):
        """
        Start a long running process in the background.

        Returns:
            Channel: A paramiko handle to the running process.
        """

        return self.ssh.exec_command(cmd)

    def close(self):
        """
        Close ssh session.
        """
        self.ssh.close()

    def exists(self, path):
        """
        Check if a file is on the remote system.

        Args:
            path (str): The path to check.

        Returns:
            bool: True if the path exists.
        """
        _, _, err = self.ssh.exec_command('ls {}'.format(path))
        err_body = err.readlines()
        assert not err_body, err_body
        return not err.readlines()
