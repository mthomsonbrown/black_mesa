variable "public_key_name" {
    description = "Name of the AWS key to use for this instance."
    default = "fancy-key-name"
}

variable "private_key_path" {
    description = "Path to the private ssh key to access the instance."
    default = "/home/mike/source_code/black_mesa/cruft/fancy-key-name.pem"
}

variable "user" {
    description = "The name of the user to log into the nodes."
    default = "ubuntu"
}

variable "ami" {
    description = "The ami to use for the nodes."
    default = "ami-08b314ce48a790a19"
}

variable "type" {
    description = "The type of instance to create."
    default = "t2.medium"
}

variable "cidr" {
    description = "The cidr block to use for the vpc and subnet"
    default = "192.168.0.0/16"
}

variable "cluster_tag" {
    description = "The tag used to identify cluster resources."
    default = "kubernetes.io/cluster/mr_cluster"
}

provider "aws" {
    region = "us-east-1"
}

resource "aws_vpc" "kuber_net" {
    cidr_block = var.cidr

    tags = {
        Name = "kuber-net"
        (var.cluster_tag) = "owned"
    }
    enable_dns_hostnames = true
}

resource "aws_subnet" "suber_net" {
    vpc_id = "${aws_vpc.kuber_net.id}"
    cidr_block = var.cidr

    availability_zone = "us-east-1d"
    tags = {
        Name = "kuber-subnet"
        (var.cluster_tag) = "owned"
    }
}

resource "aws_internet_gateway" "kuber_gate" {
    vpc_id = "${aws_vpc.kuber_net.id}"
}

resource "aws_route_table" "kuber_route" {
    vpc_id = "${aws_vpc.kuber_net.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.kuber_gate.id}"
    }
    tags = {
        (var.cluster_tag) = "owned"
    }
}

resource "aws_route_table_association" "kuber_sociate" {
    subnet_id = "${aws_subnet.suber_net.id}"
    route_table_id = "${aws_route_table.kuber_route.id}"
}

resource "aws_security_group" "kuber_group" {
    vpc_id = "${aws_vpc.kuber_net.id}"
    name = "kuber-group"

    tags = {
        (var.cluster_tag) = "owned"
    }

    # Allow all outbound
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # Allow all internal
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        self = true
    }

    # Allow all traffic from everything
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # Allow inbound traffic to the port used by Kubernetes API HTTPS
    ingress {
        from_port = 6443
        to_port = 6443
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_ebs_volume" "kuber_volume" {
    availability_zone = "us-east-1d"
    size = 10

    tags = {
        Name = "kuber-volume"
        (var.cluster_tag) = "owned"
    }
}

resource "aws_instance" "master_node" {
    ami = var.ami
    instance_type = var.type
    key_name = var.public_key_name

    tags = {
        Name = "master-node"
        (var.cluster_tag) = "owned"
    }

    subnet_id = "${aws_subnet.suber_net.id}"
    private_ip = "${cidrhost(var.cidr, 256)}"
    associate_public_ip_address = true

    availability_zone = "us-east-1d"

    vpc_security_group_ids = ["${aws_security_group.kuber_group.id}"]

    iam_instance_profile = "k8s_master_role"

    # Make sure the system is ready to accept ssh commands, before invoking ansible
    provisioner "remote-exec" {
        inline = ["echo 'system ready'"]

        connection {
            type = "ssh"
            user = var.user
            host = self.public_ip
            private_key = file(var.private_key_path)
        }
    }

    provisioner "local-exec" {
        working_dir = "../ansible"
        command = "ansible-playbook -i '${self.public_ip},' --private-key ${var.private_key_path} ./master-playbook.yaml --extra-vars 'private_dns=${self.private_dns} public_dns=${self.public_dns}'"
    }
}

resource "aws_instance" "worker_nodes" {
    count = 2
    ami = var.ami
    instance_type = var.type
    key_name = var.public_key_name

    subnet_id = "${aws_subnet.suber_net.id}"
    private_ip = "${cidrhost(var.cidr, 10 + count.index)}"
    associate_public_ip_address = true

    availability_zone = "us-east-1d"

    vpc_security_group_ids = ["${aws_security_group.kuber_group.id}"]

    tags = {
        Name = "worker-node-${count.index}"
        (var.cluster_tag) = "owned"
    }

    iam_instance_profile = "k8s_master_role"

    # Make sure the system is ready to accept ssh commands, before invoking ansible
    provisioner "remote-exec" {
        inline = ["echo 'system ready'"]

        connection {
            type = "ssh"
            user = "ubuntu"
            host = self.public_ip
            private_key = file(var.private_key_path)
        }
    }

    provisioner "local-exec" {
        working_dir = "../ansible"
        command = "ansible-playbook -i '${self.public_ip},' --private-key ${var.private_key_path} ./worker-playbook.yaml --extra-vars 'private_dns=${self.private_dns}'"
    }
}

output "private_key_path" {
    value = var.private_key_path
}

output "user" {
    value = var.user
}
