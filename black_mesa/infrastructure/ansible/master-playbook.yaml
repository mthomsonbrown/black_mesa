---
- hosts: all
  become: true
  tasks:
  - name: Install packages that allow apt to be used over HTTPS
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg-agent
      - software-properties-common

  - name: Add an apt signing key for Docker
    apt_key:
      url: https://download.docker.com/linux/ubuntu/gpg
      state: present

  - name: Add apt repository for stable version
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
      state: present

  - name: Install docker and its dependecies
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - docker-ce
      - docker-ce-cli
      - containerd.io
    notify:
      - docker status

  - name: Remove swapfile from /etc/fstab
    mount:
      name: "{{ item }}"
      fstype: swap
      state: absent
    with_items:
      - swap
      - none

  - name: Disable swap
    command: swapoff -a
    when: ansible_swaptotal_mb > 0

  - name: Add an apt signing key for Kubernetes
    apt_key:
      url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
      state: present

  - name: Adding apt repository for Kubernetes
    apt_repository:
      repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
      state: present
      filename: kubernetes.list

  - name: Install Kubernetes binaries
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
        - kubelet
        - kubeadm
        - kubectl

  - name: Modify the cluster config file with a template
    template:
      src: k8s_master_config.yaml
      dest: /tmp/k8s_master_config.yaml
      owner: root
      group: root
      mode: 0777

  - name: Set hostname to internal dns
    command: hostnamectl set-hostname {{ private_dns }}

  - name: Initialize the Kubernetes cluster using kubeadm
    command: kubeadm init --config /tmp/k8s_master_config.yaml

  - name: create .kube directory
    become: yes
    become_user: ubuntu
    file:
      path: $HOME/.kube
      state: directory
      mode: 0755

  - name: copy admin.conf to user's kube config
    copy:
      src: /etc/kubernetes/admin.conf
      dest: /home/ubuntu/.kube/config
      remote_src: yes
      owner: ubuntu

  - name: Install calico pod network
    become: false
    command: kubectl create -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml

  - name: Copy coredns configmap to a file
    become: yes
    become_user: ubuntu
    shell: echo | kubectl get configmap coredns -n kube-system -o yaml >> /home/ubuntu/dns_config_map.yaml

  - name: Update configmap with new forward DNS address
    become: yes
    become_user: ubuntu
    replace:
      path: /home/ubuntu/dns_config_map.yaml
      regexp: '( +forward \.)( .+)'
      replace: '\1 8.8.8.8'

  - name: Patch coredns configmap with updated file
    become: yes
    become_user: ubuntu
    shell: kubectl patch configmap/coredns -n kube-system --type merge --patch "$(cat /home/ubuntu/dns_config_map.yaml)"

  - name: Generate join command
    command: kubeadm token create --print-join-command
    register: join_command

  - name: Copy join command to local file
    local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"
    become: false

  - name: Copy docker config to access dockerhub
    copy: src="{{ lookup('env', 'HOME') }}/.docker/config.json" dest=/tmp/config.json mode=0777

  - name: Create dockerhub access secret
    become: false
    command: kubectl create secret generic hubsec --from-file=.dockerconfigjson=/tmp/config.json --type=kubernetes.io/dockerconfigjson

  handlers:
    - name: docker status
      service: name=docker state=started
