import os
from pathlib import Path

import boto3

from black_mesa.environment import Environment
from black_mesa.logger import Logger


class AWSController(object):
    """
    Interact with AWS to manage EC2 instances.
    """
    def __init__(self):
        self.log = Logger(__name__).logger
        config = self.load_config()
        self.workbench = config['workbench']
        self.env = config['aws']
        self.instances = []

        self.ec2 = boto3.resource(
            'ec2',
            region_name=self.env['region_name'],
            aws_access_key_id=self.env['access_key_id'],
            aws_secret_access_key=self.env['secret_access_key'])
        self.client = boto3.client(
            'ec2',
            region_name=self.env['region_name'],
            aws_access_key_id=self.env['access_key_id'],
            aws_secret_access_key=self.env['secret_access_key'])

    @property
    def keypath(self):
        """
        Returns:
            (Path) key path from the config file.
        """
        return Path(self.workbench) / self.env['ec2_key']['filename']

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'aws': {
                'target_ami': '<The AMI you want to deploy instances with>',
                'region_name': '<The region you want to use for these images>',
                'access_key_id': '<Access key id>',
                'secret_access_key': '<Access key associated with key id>',
                'ec2_key': {
                    'name': 'fancy-key-name',
                    'filename': 'fancy-key-name.pem',
                },
                'instance_type': 't2.micro',
                'instance_name': 'Automated Test'
            }
        })
        return env.load_config()

    def create_ec2_key(self):
        """Create new key pair on AWS."""
        with open(
                Path(self.workbench, self.env['ec2_key']['filename']),
                'w'
        ) as outfile:
            key_pair = self.ec2.create_key_pair(  # pylint: disable=no-member
                KeyName=self.env['ec2_key']['name'])
            outfile.write(str(key_pair.key_material))

            os.chmod(
                os.path.join(self.workbench, self.env['ec2_key']['filename']),
                0o400
            )

    def connect_to_instance(self, instance_id):
        """
        Retrieve the login information for a running EC2 instance.

        Args:
            instance_id (str): The ID of the EC2 instance to connect to.

        Returns:
            str: The IP of the new system.
            str: Path to the private key for this instance.
        """
        instance = self.ec2.Instance(instance_id)  # pylint: disable=no-member
        self.log.debug(f"Instance ip is {instance.public_ip_address}")
        return (instance.public_ip_address,
                os.path.join(self.workbench, self.env['ec2_key']['filename']))

    def create_instance(self):
        """
        Creates an EC2 instance based on the current parameters.

        Returns:
            str: The IP of the new system.
            str: Path to the private key for this instance.
        """
        if not os.path.isfile(os.path.join(self.workbench,
                                           self.env['ec2_key']['filename'])):
            print("Key not found.  making a new one.")
            self.create_ec2_key()

        instances = self.ec2.create_instances(  # pylint: disable=no-member
            ImageId=self.env['target_ami'],
            MinCount=1,
            MaxCount=1,
            KeyName=self.env['ec2_key']['name'],
            InstanceType=self.env['instance_type'],
            TagSpecifications=[{
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': self.env['instance_name']
                    }
                ]
            }]
        )
        self.log.debug(f"Instances are {instances}")
        if len(instances) != 1:
            raise IndexError(
                "Requested one instance but got {}".format(len(instances)))
        instance = instances[0]
        self.instances.append(instance)
        self.log.debug("Waiting for instance to reach running state")
        instance.wait_until_running()

        # Hack: IP address not returned unless you reconnect to instance
        instance = self.ec2.Instance(instance.id)  # pylint: disable=no-member
        self.log.debug(f"Instance ip is {instance.public_ip_address}")
        return (instance.public_ip_address,
                os.path.join(self.workbench, self.env['ec2_key']['filename']))

    def terminate_instances(self):
        """Terminate all instances owned by this object."""
        instance_ids = []
        for instance in self.instances:
            instance_ids.append(instance.id)
        self.ec2.instances.filter(InstanceIds=instance_ids).terminate()  # pylint: disable=no-member
