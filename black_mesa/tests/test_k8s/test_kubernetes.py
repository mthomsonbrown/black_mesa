import shutil
from pathlib import Path
from polling import poll

import requests

import pytest

from black_mesa.k8s_controller import K8SController

from black_mesa.fixtures.k8s import K8S, Cluster


class TestKubernetes(K8S):

    """
    Environmental/Integration tests for containerized AKM
    running inside a Kubernetes cluster.
    """

    @pytest.mark.sanity
    def hello_k8s_test(self, cluster: Cluster):
        """
        Steps:
            - Spin up a cluster using ansible and terraform.
            - Generate a YAML file describing a pod containing the
                hello world container.
            - Deploy the container and a service to expose it externally.
            - Send a get request to the endpoint.

        Expect:
            - Service should return the string 'Hello Kubernetes!'
        """
        workdir, remotes, _ = cluster

        dep_path = Path('/home/ubuntu/sanity.yaml')
        service_name = "the-service"

        master = remotes['masters'][0]
        deployment = Path(__file__).with_name(dep_path.name)
        shutil.copy(deployment, workdir)
        master.upload(workdir / dep_path.name, dep_path)

        master.exec_check(f'kubectl apply -f {dep_path}')
        master.exec_check(
            'kubectl expose deployment hello-world '
            f'--type=NodePort --name={service_name}',
            ignore="already exists"
        )
        info = master.exec_check(
            f'kubectl get svc {service_name} -o json',
            json_out=True
        )
        port = info['spec']['ports'][0]['nodePort']

        timeout = 60
        print(f"polling service for up to {timeout} seconds")
        response = poll(
            lambda: requests.get(f'http://{master.sys_ip}:{port}'),
            step=1,
            timeout=timeout,
            ignore_exceptions=(requests.exceptions.ConnectionError)
        )
        assert response.content.decode('utf-8') == 'Hello Kubernetes!'

        print(remotes)

    def akm_mk1_test(self, cluster: Cluster):
        """
        WIP test to demonstrate the current state of progress.

        The container can be pulled and launched in a pod, but
        persistent storage, keep alive, and port assignment hasn't
        been completed yet.
        """
        workdir, remotes, _ = cluster

        pod_conf = Path('/home/ubuntu/akm_mk1.yaml')

        master = remotes['masters'][0]
        deployment = Path(__file__).with_name(pod_conf.name)
        shutil.copy(deployment, workdir)
        master.upload(workdir / pod_conf.name, pod_conf)

        master.exec_check(f'kubectl apply -f {pod_conf}')

        def pod_is_ready():
            dat = master.exec_check(
                'kubectl get pod akm -o json',
                json_out=True
            )
            for status in dat['status']['containerStatuses']:
                if status['name'] != 'akm':
                    continue
                if 'terminated' not in status['state']:
                    return False
                if status['state']['terminated']['reason'] != 'Completed':
                    print(dat)
                    raise AssertionError('Container terminated abnormally')
                return True

        timeout = 600
        print(f"polling for pod success for up to {timeout} seconds")
        poll(
            pod_is_ready,
            step=1,
            timeout=timeout
        )

        print(remotes)

    def lock_and_load_test(self, cluster: Cluster):
        """Set up a cluster and push our infrastructure files to it."""
        workdir, remotes, volume_id = cluster
        master = remotes['masters'][0]

        yamls = {
            'pv': 'pv.yaml',
            'pvc': 'pvc.yaml',
            'pod': 'akm_mk2.yaml',
            'port': 'port_service.yaml'
        }

        # copy template files to temp directory
        for _, yaml_file in yamls.items():
            shutil.copy(Path(__file__).with_name(yaml_file), workdir)

        # modify template files
        self.update_volume_id(volume_id, workdir / yamls['pv'])

        # copy template files to cluster
        up_dir = Path('/home/ubuntu/')
        for _, yaml_file in yamls.items():
            master.upload(workdir / yaml_file, up_dir / yaml_file)

        print(remotes)

    def fully_automatic_test(self, cluster: Cluster):
        """
        Set up akm in a k8s cluster, create a key, and provision a key from
        outside the cluster.
        """
        workdir, remotes, volume_id = cluster
        master = remotes['masters'][0]

        yamls = {
            'pv': 'pv.yaml',
            'pvc': 'pvc.yaml',
            'pod': 'akm_mk2.yaml',
            'port': 'port_service.yaml'
        }

        # copy template files to temp directory
        for _, yaml_file in yamls.items():
            shutil.copy(Path(__file__).with_name(yaml_file), workdir)

        # modify template files
        self.update_volume_id(volume_id, workdir / yamls['pv'])

        # copy template files to cluster
        up_dir = Path('/home/ubuntu/')
        for _, yaml_file in yamls.items():
            master.upload(workdir / yaml_file, up_dir / yaml_file)

        # Apply yaml files
        for _, yaml_file in yamls.items():
            master.exec_check(f'kubectl apply -f {up_dir / yaml_file}')

        # Do some akm key things
        k8s = K8SController(remotes['masters'], remotes['workers'])
        admin = k8s.python_sdk_controller.akm_admin
        client = k8s.python_sdk_controller.akm_client
        admin.create_aes_key('test_key')
        admin.assert_success()
        client.get_aes_key('test_key')
        client.assert_success()

    def provisioned_cluster_test(self, provisioned_cluster: K8SController):
        """
        Set up akm in a k8s cluster, and install bootstrap
        """
        cluster = provisioned_cluster

        # Do some akm key things
        admin = cluster.python_sdk_controller.akm_admin
        client = cluster.python_sdk_controller.akm_client
        admin.create_aes_key('test_key')
        admin.assert_success()
        client.get_aes_key('test_key')
        client.assert_success()
