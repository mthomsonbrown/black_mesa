"""
Tests of asa running in a k8s cluster.
"""
import pytest

from black_mesa.fixtures.k8s import K8S
from black_mesa.k8s_controller import K8SController


@pytest.mark.asa
class TestASA(K8S):
    def asa_sanity_test(self, asa_cluster: K8SController):
        """
        Steps:
            Make sure akmd and asa are responding to basic requests.

        Expect:
            They should talk back!
        """
        # Do some akm key things
        admin = asa_cluster.python_sdk_controller.akm_admin
        client = asa_cluster.python_sdk_controller.akm_client
        admin.create_aes_key('test_key')
        admin.assert_success()
        client.get_aes_key('test_key')
        client.assert_success()

        resp = asa_cluster.exec('kubectl exec -it akm -- akm-exec version')
        # TODO: When this breaks because of a version change, make asa's version
        # reporting and this test more robust.
        assert '1.1' in resp.decode('utf-8').strip('\n')
