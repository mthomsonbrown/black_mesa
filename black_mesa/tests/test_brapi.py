from http import HTTPStatus as http_codes
from pathlib import Path

import requests
import pytest

from black_mesa.fixtures.core import Core


PORT = '8000'


@pytest.mark.brapi
class TestBRAPI(Core):

    license_file = Path('/var/lib/townsend/akm/License.txt')

    def can_generate_license_from_server_test(self, bs_akm):
        res = requests.post(f'http://{bs_akm.ip}:{PORT}/license/')
        assert res.status_code == http_codes.OK
        bs_akm.remote.exec_check(f'ls {self.license_file}')

    def license_imported_from_argument_test(self, bs_akm):
        lic_arg = 'a' * 128
        res = requests.post(
            f'http://{bs_akm.ip}:{PORT}/license/',
            data={'license_string': lic_arg}
        )
        assert res.status_code == http_codes.OK
        bs_akm.remote.exec_check(f'ls {self.license_file}')
        installed = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed == lic_arg

    def license_not_overwritten_by_server_call_test(self, bs_akm):
        res = requests.post(f'http://{bs_akm.ip}:{PORT}/license/')
        assert res.status_code == http_codes.OK
        installed1 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        res = requests.post(f'http://{bs_akm.ip}:{PORT}/license/')
        assert res.status_code == http_codes.FORBIDDEN
        installed2 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed1 == installed2

    def license_not_overwritten_by_string_argument_test(self, bs_akm):
        lic_arg = 'c' * 128
        res = requests.post(f'http://{bs_akm.ip}:{PORT}/license/')
        assert res.status_code == http_codes.OK
        installed1 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        res = requests.post(
            f'http://{bs_akm.ip}:{PORT}/license/',
            data={'license_string': lic_arg}
        )
        assert res.status_code == http_codes.FORBIDDEN
        installed2 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed1 == installed2

    def too_long_license_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 129
        res = requests.post(
            f'http://{bs_akm.ip}:{PORT}/license/',
            data={'license_string': lic_arg}
        )
        assert res.status_code == http_codes.INTERNAL_SERVER_ERROR

    def too_short_license_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 127
        res = requests.post(
            f'http://{bs_akm.ip}:{PORT}/license/',
            data={'license_string': lic_arg}
        )
        assert res.status_code == http_codes.INTERNAL_SERVER_ERROR
