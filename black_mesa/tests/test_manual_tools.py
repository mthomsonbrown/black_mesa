"""Setup akms in various configurations to perform manual tests."""
from pathlib import Path

import pytest

from black_mesa.logger import Logger
from black_mesa.akm_controller import AKMController
from black_mesa.environment import Environment

from black_mesa.fixtures.core import Core


LOG = Logger(__name__).logger


class TestManualTools(Core):
    target_ip = '3.95.198.167'
    default_password = 'OOHXPq6r530N6re'
    @pytest.mark.init
    def test_init_aws(self, fresh_akm: AKMController):
        assert True
        print(f"Initialized system: {fresh_akm.ip}")

    @pytest.mark.upload
    def test_upload(self):
        env = Environment().load_config()
        akm = AKMController(
            self.target_ip,
            key=Path(env['akm']['local_ssh_pubkey_path'], env['aws']['ec2_key']['filename'])
        )
        akm.remote.upload(
            Path('/home/mike/source_code/plank/deb_dist/python3-plank_1.0.0-0ubuntu1_all.deb'),
            Path('/home/admin/uploads/')
        )
        akm.remote.upload(
            Path('/home/mike/source_code/akm2json/deb_dist'
                 '/python3-akm2json_1.0.0-0ubuntu1_all.deb'),
            Path('/home/admin/uploads/')
        )
        print(f"Uploaded thing to: {akm.ip}")

    @pytest.mark.get_key
    def test_get_key(self):
        key_name = 'EKM256'
        akm = AKMController(
            self.target_ip,
            password=self.default_password
            )
        akm.python_sdk_controller.akm_client.get_aes_key(key_name)
        akm.python_sdk_controller.akm_client.assert_success()

    @pytest.mark.az_init
    def test_init_az(self, azure):  # pylint: disable=W0613
        assert True

    @pytest.mark.legacy_bs
    def test_legacy_bootstrap(self, legacy_bs_akm):  # pylint: disable=W0613
        assert True

    @pytest.mark.boot_man
    def test_boot_man(self, bs_akm):
        assert True
        LOG.info(f"IP: {bs_akm.ip}")
