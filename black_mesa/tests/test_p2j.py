import time
from pathlib import Path
import shutil

import pytest

from black_mesa.environment import Environment

from black_mesa.fixtures.core import Core


@pytest.mark.akm2json
class TestP2J(Core):
    akm_error_log_line = (
        "<20190426 22:00:14> <0000000001> AKM startup, version <4.6.1.1850.1>"
    )
    akm_audit_log_line = (
        "2019-04-26,22:00:18,CreateRsaKeyPair,akm,4.6.1.1850.1,127.0.0.1:6001,"
        "127.0.0.1:34856,Key <RSA4096>? private instance "
        "</qRX1l8/tV1jy8baDWLoGQ==>? public instance "
        "<UB6/6I6/RJql7AKlWY7NWA==> created with access <All>,admin1,"
        "akm_admin,Admin,N,14,skMTickgw9QaZhxqu4Imu4fD0Hb3jx+uTAGRioHQ+TU="
    )
    bootstrap_log_line = (
        ">>create_p12(/var/lib/townsend/akm/rootCA/client_certs/admin2"
        "/20190426/AKMAdminCertificateAndPrivateKey.p12)"
    )

    workbench = Path(Environment().load_config()['workbench'])
    logger_output = workbench / 'report'
    logger_err = workbench / 'err_report'
    local_logs = workbench / 'log_test/'

    @pytest.fixture(scope='function')
    def local_paths(self, worker_id):
        """
        Create function specific names for local files and directories.
        Delete and recreate directories if they already exist.

        Returns:
            Path: location for the log_out file.
            Path: location for the log_err file.
            Path: location to store the downloaded logs from the akm.
        """

        log_out = self.logger_output.with_name(
            f"{self.logger_output.name}-{worker_id}"
        )
        log_err = self.logger_err.with_name(
            f"{self.logger_err.name}-{worker_id}"
        )
        log_dir = self.local_logs.with_name(
            f"{self.local_logs.name}-{worker_id}"
        )
        if log_out.exists():
            log_out.unlink()
        if log_err.exists():
            log_err.unlink()
        if log_dir.exists():
            shutil.rmtree(log_dir)
        log_dir.mkdir(parents=True)
        return log_out, log_err, log_dir

    def one_log_is_detected_test(self, bs_akm, local_paths):
        log_out, *_ = local_paths

        logger = bs_akm.json_logger
        logger.start(log_out)
        time.sleep(1)
        bs_akm.remote.exec_check(
            f'echo "{self.bootstrap_log_line}" >> '
            f'{self.bootstrap_log_path}',
            verbose=True
        )
        logger.stop()
        assert len(open(log_out).readlines()) == 1

    @pytest.mark.xfail
    def logs_read_without_delay_test(self, bs_akm, local_paths):
        log_out, *_ = local_paths

        logger = bs_akm.json_logger

        num = 5
        logger.start(log_out)
        for idx in range(num):
            bs_akm.remote.exec_check(
                f'echo "{"{}{}".format(self.bootstrap_log_line, idx)}" >> '
                f'{self.bootstrap_log_path}',
                verbose=True
            )
        logger.stop()
        assert len(open(log_out).readlines()) == num

    def bs_init_logs_captured_test(self, bs_akm, local_paths):
        log_out, _, local_log_dir = local_paths
        logger = bs_akm.json_logger

        logger.start(log_out)
        # print("spinup")
        # time.sleep(5)

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')
        # print("spindown")
        # time.sleep(1)

        logger.stop()

        remote = bs_akm.remote
        log_files = remote.list_dir(str(self.log_dir))

        for file_ in log_files:
            remote.download(self.log_dir / file_, local_log_dir)

        sum_log_lines = 0
        for file_ in local_log_dir.iterdir():
            cur_log_lines = open(local_log_dir / file_).readlines()
            if 'bootstrap' in file_.name:
                cur_log_lines = [
                    line for line in cur_log_lines if line.strip('>\n')
                ]

            sum_log_lines += len(cur_log_lines)

        assert len(open(log_out).readlines()) == sum_log_lines

    def akmd_activity_captured_test(self, bs_akm, local_paths):
        log_out, log_err, _ = local_paths

        logger = bs_akm.json_logger

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')

        pcon = bs_akm.python_sdk_controller

        key_name_prefix = 'testkey'
        key_names = {f'{key_name_prefix}{num}' for num in range(10)}

        logger.start(log_out, log_err)

        for name in key_names:
            pcon.akm_admin.create_aes_key(name)

        logger.stop()

        discovered_names = set()

        for line in log_out.read_text().splitlines():
            if key_name_prefix in line:
                for name in key_names:
                    if name in line:
                        discovered_names.add(name)

        assert not key_names - discovered_names

    def akmerror_goes_to_stderr_test(self, bs_akm, local_paths):
        log_out, log_err, _ = local_paths

        logger = bs_akm.json_logger

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')

        pcon = bs_akm.python_sdk_controller

        logger.start(log_out, log_err)

        pcon.akm_client.get_aes_key('testkey')

        logger.stop()

        assert 'testkey' in open(log_err).read()

    def rotated_logs_identified_and_ignored_test(self, bs_akm, local_paths):
        log_out, log_err, _ = local_paths

        logger = bs_akm.json_logger

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')

        meg = 1_048_576
        bs_akm.remote.exec_check(
            f"dd if=/dev/urandom of={self.audit_log_path} "
            f"count=11 bs={meg}",
            verbose=True,
            ignore=' '
        )

        logger.start(log_out, log_err)

        bs_akm.rotate_logs()

        logger.stop()

        assert not log_err.exists()

    def logging_continues_after_rotate_test(self, bs_akm, local_paths):
        log_out, log_err, _ = local_paths

        logger = bs_akm.json_logger

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')

        pcon = bs_akm.python_sdk_controller

        meg = 1_048_576
        bs_akm.remote.exec_check(
            f"dd if=/dev/urandom of={self.audit_log_path} "
            f"count=11 bs={meg}",
            verbose=True,
            ignore=' '
        )

        logger.start(log_out, log_err)

        bs_akm.rotate_logs()

        pcon.akm_admin.create_aes_key('testkey')

        logger.stop()

        assert 'testkey' in open(log_out).read()

    def no_read_access_reported_test(self, bs_akm, local_paths):
        log_out, log_err, _ = local_paths

        logger = bs_akm.json_logger

        bs_con = bs_akm.legacy_bootstrap_controller
        bs_con.initialize('drogon')

        bs_akm.remote.exec_check(
            'sudo chmod -r /var/log/townsend/akmaudit.log'
        )

        pcon = bs_akm.python_sdk_controller
        logger.start(log_out, log_err)

        pcon.akm_admin.create_aes_key('testkey')

        logger.stop()

        assert 'Permission denied' in open(log_err).read()
