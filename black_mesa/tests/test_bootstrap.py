from pathlib import Path

import pytest

from black_mesa.fixtures.core import Core


@pytest.mark.bootstrap
class TestBootstrap(Core):

    """
    Test bootstrap through the CLI.

    Tests only for bootstrap 3+
    """

    license_file = Path('/var/lib/townsend/akm/License.txt')
    uploads_dir = Path('/home/admin/uploads/')
    license_error_file = Path('/home/admin/ERR_License.txt')

    def can_generate_license_from_server_test(self, bs_akm):
        res = bs_akm.bootstrap_controller.cli('license')
        assert res['success']
        bs_akm.remote.exec_check(f'ls {self.license_file}')

    def license_imported_from_argument_test(self, bs_akm):
        lic_arg = 'a' * 128
        res = bs_akm.bootstrap_controller.cli(f'license --string {lic_arg}')
        assert res['success']
        bs_akm.remote.exec_check(f'ls {self.license_file}')
        installed = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed == lic_arg

    def license_imported_from_file_test(self, bs_akm):
        lic_arg = 'b' * 128
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.txt"}'
        )
        res = bs_akm.bootstrap_controller.cli('license')
        assert res['success']
        installed = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed == lic_arg

    def license_not_overwritten_by_server_call_test(self, bs_akm):
        res = bs_akm.bootstrap_controller.cli('license')
        assert res['success']
        installed1 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        res = bs_akm.bootstrap_controller.cli('license')
        assert not res['success']
        installed2 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed1 == installed2

    def license_not_overwritten_by_string_argument_test(self, bs_akm):
        lic_arg = 'c' * 128
        res = bs_akm.bootstrap_controller.cli('license')
        assert res['success']
        installed1 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        res = bs_akm.bootstrap_controller.cli(f'license --string {lic_arg}')
        assert not res['success']
        installed2 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed1 == installed2

    def license_not_overwritten_by_file_upload_test(self, bs_akm):
        lic_arg = 'd' * 128
        res = bs_akm.bootstrap_controller.cli('license')
        assert res['success']
        installed1 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.txt"}'
        )
        res = bs_akm.bootstrap_controller.cli('license')
        assert not res['success']
        installed2 = bs_akm.remote.exec_check(
            f'cat {self.license_file}'
        ).decode('utf-8')
        assert installed1 == installed2

    def too_long_license_file_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 129
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.txt"}'
        )
        res = bs_akm.bootstrap_controller.cli('license')
        assert not res['success']
        with pytest.raises(AssertionError):
            bs_akm.remote.exec_check(f'ls {self.license_file}')
        err_file = bs_akm.remote.exec_check(
            f'cat {self.license_error_file}'
        ).decode('utf-8')
        assert err_file == lic_arg

    def too_short_license_file_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 127
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.txt"}'
        )
        res = bs_akm.bootstrap_controller.cli('license')
        assert not res['success']
        with pytest.raises(AssertionError):
            bs_akm.remote.exec_check(f'ls {self.license_file}')
        err_file = bs_akm.remote.exec_check(
            f'cat {self.license_error_file}'
        ).decode('utf-8')
        assert err_file == lic_arg

    def too_long_license_argument_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 129
        res = bs_akm.bootstrap_controller.cli(f'license --string {lic_arg}')
        assert not res['success']
        with pytest.raises(AssertionError):
            bs_akm.remote.exec_check(f'ls {self.license_file}')
        err_file = bs_akm.remote.exec_check(
            f'cat {self.license_error_file}'
        ).decode('utf-8')
        assert err_file == lic_arg

    def too_short_license_argument_not_accepted_test(self, bs_akm):
        lic_arg = 'e' * 127
        res = bs_akm.bootstrap_controller.cli(f'license --string {lic_arg}')
        assert not res['success']
        with pytest.raises(AssertionError):
            bs_akm.remote.exec_check(f'ls {self.license_file}')
        err_file = bs_akm.remote.exec_check(
            f'cat {self.license_error_file}'
        ).decode('utf-8')
        assert err_file == lic_arg

    def too_many_licenses_in_uploads(self, bs_akm):
        lic_arg = 'f' * 128
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.txt"}'
        )
        bs_akm.remote.exec_check(
            f'echo -n {lic_arg} > {self.uploads_dir / "License.two.txt"}'
        )
        res = bs_akm.bootstrap_controller.cli('license')
        assert not res['success']
