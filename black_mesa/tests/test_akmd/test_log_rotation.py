from datetime import timedelta

import pytest

from black_mesa.log_controller import LogController

from black_mesa.fixtures.akmd_fixtures import AKMDFixtures


@pytest.mark.regression
class TestLogRotation(AKMDFixtures):

    """
    Tests exercising the log rotation component. This should probably
    extend to testing basic logging functionality as well.

    Basic Idea:
        - If a log hits 10 MB, calling log rotate should rotate it
        - If a log is less than 10 MB, calling log rotate should not rotate it
        - Rotating a log while akmd is running shouldn't cause problems
        - Rotating a log while akmd is off shouldn't cause problems when you
            turn it back on
        - Rotated logs named with today's date prevent log rotation
        - Rotated logs with a different date don't prevent rotation
    Logging generic test ideas:
        - Writing to the log shouldn't prevent akmd from writing to the log
        - Deleting a log file shouldn't prevent akmd from creating a new log
            and writing to that.
    """

    def log_rotates_test(self, akmd_akm, log_path_bs_fail):
        """
        Steps:
            - Fill the log file with 10 MB of data
            - Remove previous logs with the same name
            - Call log rotate
        Expect:
            Log file should have rotated
        """
        log_path = log_path_bs_fail

        log_con = LogController(akmd_akm, log_path)
        log_con.fill_file(10).rem_log_archive().rotate_logs()
        assert log_con.archive_exists()

    def small_log_does_not_rotate_test(self, akmd_akm, log_path):
        """
        Steps:
            - Fill the log with 9 MB of data
            - Remove previous logs with the same name
            - Call log rotate
        Expect:
            Log file should not have rotated
        """
        log_con = LogController(akmd_akm, log_path)
        log_con.fill_file(9).rem_log_archive().rotate_logs()
        assert not log_con.archive_exists()

    def running_akmd_can_error_log_after_rotate_test(
            self, akmd_akm
    ):
        """
        Steps:
            - Turn on akmd
            - Rotate a log file
            - Do a thing with akmd to generate a new log in that file
        Expect:
            The new log file should be populated with akmd's log message
        """
        akmd_akm.turn_on_akmd()

        log_con = LogController(akmd_akm, self.err_log_path)
        log_con.perform_rotation()

        admin = akmd_akm.python_sdk_controller.akm_admin
        admin.activate_key('missing')
        assert not log_con.empty()

    def stopped_akmd_can_log_after_rotate_test(
            self, akmd_akm
    ):
        """
        Steps:
            - Turn off akmd
            - Rotate a log file
            - Turn on akmd
        Expect:
            Logs to be present in the new log file
        """
        akmd_akm.turn_off_akmd()

        log_con = LogController(akmd_akm, self.err_log_path)
        log_con.perform_rotation()

        akmd_akm.turn_on_akmd()
        assert not log_con.empty()

    def log_rotate_does_not_overwrite_test(self, akmd_akm):
        """
        Steps:
            - Create a file named as a log backup with the current date
            - Try to run logrotate
        Expect:
            File is not overwritten
        """
        run = akmd_akm.remote.exec_check
        message = 'i was here first'

        log_con = LogController(akmd_akm, self.err_log_path)
        rotated_path = log_con.rotated_path
        run(f'echo "{message}" > {rotated_path}')

        log_con.fill_file().rotate_logs()
        content = run(f'cat {rotated_path}').decode('utf-8')
        assert message in content

    def log_rotate_works_with_different_date_archive_test(self, akmd_akm):
        """
        Steps:
            - Create a file named as a log backup with yesterday's date
            - Try to run logrotate
        Expect:
            New archive is made for today's date
        """
        remote = akmd_akm.remote

        log_con = LogController(akmd_akm, self.err_log_path)
        old_rotated_path = log_con.calculate_rotated_path(
            remote.date() + timedelta(days=-1)
        )
        remote.exec_check(f'touch {old_rotated_path}')

        log_con.perform_rotation()

    def akmd_write_after_manual_write_err_test(self, akmd_akm):
        """
        Steps:
            - Turn on akmd
            - clear the error log in case there's 10MB of stuff in
                there this test will take forever parsing it...
            - Write to the error log
            - Have akmd write to the error log
        Expect:
            AKMD wrote to the error log
        """
        msg = "stuff"
        run = akmd_akm.remote.exec_check
        akmd_akm.turn_on_akmd()
        run(f'echo {msg} > {self.err_log_path}')
        akmd_akm.python_sdk_controller.akm_admin.activate_key('missing')
        dat = run(f'cat {self.err_log_path}').decode('utf-8')
        assert msg not in dat.split()[-1]

    @pytest.mark.xfail(strict=True, run=False)  # TODO: add bug number
    def akmd_recreates_deleted_logfile_test(self, akmd_akm):
        """
        Steps:
            - Turn on akmd
            - Delete error log
            - Have akmd write to the error log
        Expect:
            AKMD wrote to the error log
        """
        run = akmd_akm.remote.exec_check
        akmd_akm.turn_on_akmd()
        run(f'sudo rm {self.err_log_path}*')
        dat = run(f'ls {self.log_dir}')
        assert self.err_log_path.name not in dat.decode('utf-8')
        akmd_akm.python_sdk_controller.akm_admin.activate_key('missing')
        assert self.err_log_path.name in dat.decode('utf-8')

    @pytest.mark.xfail(strict=True, run=False)  # TODO: add bug number
    def akmd_recreates_deleted_logfile_at_startup_test(self, akmd_akm):
        """
        Steps:
            - Turn on akmd
            - Delete error log
            - Turn off akmd
            - Turn on akmd
        Expect:
            AKMD recreated the error log
        """
        run = akmd_akm.remote.exec_check
        akmd_akm.turn_on_akmd()
        run(f'sudo rm {self.err_log_path}*')
        dat = run(f'ls {self.log_dir}')
        assert self.err_log_path.name not in dat.decode('utf-8')
        akmd_akm.turn_off_akmd()
        akmd_akm.turn_on_akmd()
        assert self.err_log_path.name in dat.decode('utf-8')
