import os
import base64

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

import pytest

from akm_sdk.akm import Padding
from akm_sdk.status_codes import Status

from black_mesa.fixtures.akmd_fixtures import AKMDFixtures


@pytest.mark.regression
class TestImportExport(AKMDFixtures):

    """
    Test import and export functionality of akmd.

    This test suite is intended as a replacement and
    extension of the import_export.txt suite in Robot.
    Once all the tests in that suite have been implemented
    here, the Robot suite can be deleted.
    """

    def import_aes_cbc_rsa_decrypt_local_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Use cryptography to create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt data with AES key using CBC mode
            - Encrypt AES key with the public RSA key
            - Import the encrypted AES key
            - Get the key back from AKM using client api
            - Decrypt the thing locally using python cryptography

        Expect:
            Import should succeed. Data should be decrypted properly.
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        user = akmd_akm.python_sdk_controller.akm_client

        iv = os.urandom(16)

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key

        # encrypt something with AES key
        cipher_text = self.do_encrypt_aes_cbc(key, iv)

        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.OAEP
        )

        # decrypt with aes key
        self.do_local_decrypt_aes_cbc(user, cipher_text, iv)

    def import_aes_wrapped_key_bytes_same_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt the AES key with the pubic RSA key
            - Import the encrypted AES key
            - Get the key back from the AKM using the client API

        Expect:
            The retrieved key bytes should be the same as the local key
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        user = akmd_akm.python_sdk_controller.akm_client

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key
        assert len(key) == 32
        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.OAEP
        )

        # Check that the keys are the same
        resp = user.get_aes_key(self.aes_key_name)
        assert resp['KeyValue'] == key

    def import_aes_ecb_rsa_decrypt_local_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Use cryptography to create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt data with AES key using ECB mode
            - Encrypt AES key with the public RSA key
            - Import the encrypted AES key
            - Get the key back from AKM using client api
            - Decrypt the thing locally using python cryptography

        Expect:
            Import should succeed. Data should be decrypted properly.
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        user = akmd_akm.python_sdk_controller.akm_client

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key

        # encrypt something with AES key
        cipher_text = self.do_encrypt_aes_ecb(key)

        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.OAEP
        )

        # use aes to decrypt the data
        self.do_local_decrypt_aes_ecb(user, cipher_text)

    def import_aes_rsa_oaep_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Use cryptography to create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt data with AES key
            - Encrypt AES key with the public RSA key
            - Import the encrypted AES key
            - Decrypt data with AKM using the imported key

        Expect:
            Import should succeed. Data should be decrypted properly.
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        encr = akmd_akm.python_sdk_controller.akm_enc

        iv = os.urandom(16)

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key

        # encrypt something with AES key
        cipher_text = self.do_encrypt_aes_cbc(key, iv)

        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.OAEP
        )

        # decrypt with imported aes key
        resp = encr.aes_cbc_decrypt(
            cipher_text.decode('latin-1'),
            iv.decode('latin-1'),
            self.aes_key_name,
            padding_flag='N'
        )
        decrypted = resp['Plaintext']

        assert decrypted == self.plain_text

    @pytest.mark.xfail(reason="AKMD-98", strict=True)
    def import_aes_rsa_sha256_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Use cryptography to create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt data with AES key
            - Encrypt AES key with the public RSA key using
                OAEP padding and a SHA256 hash
            - Import the encrypted AES key
            - Decrypt data with AKM using the imported key

        Expect:
            Import should succeed. Data should be decrypted properly.
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        encr = akmd_akm.python_sdk_controller.akm_enc

        iv = os.urandom(16)

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key

        # encrypt something with AES key
        cipher_text = self.do_encrypt_aes_cbc(key, iv)

        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.OAEP
        )

        # decrypt with imported aes key
        resp = encr.aes_cbc_decrypt(
            cipher_text.decode('latin-1'),
            iv.decode('latin-1'),
            self.aes_key_name,
            padding_flag='N'
        )
        decrypted = resp['Plaintext']

        assert decrypted == self.plain_text

    def import_aes_rsa_pkcs1_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Use cryptography to create an RSA key
            - Import the private part
            - Create an AES key
            - Encrypt data with AES key
            - Encrypt AES key with the public RSA key
            - Import the encrypted AES key
            - Decrypt data with AKM using the imported key

        Expect:
            Import should succeed. Data should be decrypted properly.
        """
        admin = akmd_akm.python_sdk_controller.akm_admin
        user = akmd_akm.python_sdk_controller.akm_client

        iv = os.urandom(16)

        # generate rsa key
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )

        # import rsa key
        priv_key_str = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        ).decode('latin-1')
        admin.import_wrapper_private_key(priv_key_str, self.wrapper_key_name)

        # create aes key
        key = rand_aes_key

        cipher = Cipher(
            algorithms.AES(key),
            modes.CBC(iv),
            backend=default_backend()
        )

        # encrypt something with AES key
        encryptor = cipher.encryptor()
        cipher_text = encryptor.update(self.plain_text) + encryptor.finalize()

        # encrypt aes key
        encrypted_aes_key = private_key.public_key().encrypt(
            key,
            padding.PKCS1v15()
        )

        # import aes key
        admin.import_aes_key(
            self.aes_key_name,
            '0256',
            aes_key_value=encrypted_aes_key.decode('latin-1'),
            aes_key_format='RSA',
            wrapper_private_key_name=self.wrapper_key_name,
            padding=Padding.PKCS1
        )

        # download aes key
        resp = user.get_aes_key(self.aes_key_name)
        assert resp['KeyValue'] == key

        # decrypt with aes key
        cipher = Cipher(
            algorithms.AES(resp['KeyValue']),
            modes.CBC(iv),
            backend=default_backend()
        )
        decryptor = cipher.decryptor()
        decrypted = decryptor.update(cipher_text) + decryptor.finalize()
        assert decrypted == self.plain_text

    def import_aes_bin_any_pad_test(
            self, pad_mode, akmd_akm, rand_aes_key
    ):
        """
        Steps:
            - Create an AES key and encrypt something
            - Import key in BIN format with each padding flag
            - Retrieve the key, decode it, and use it to decrypt the thing

        Expect:
            Thing should decrypt
        """
        with self.pcidss_off(akmd_akm):

            pad_mode = 2
            admin = akmd_akm.python_sdk_controller.akm_admin
            user = akmd_akm.python_sdk_controller.akm_client
            key_format = 'BIN'

            # create key
            key = rand_aes_key

            # encrypt data with the key
            cipher_text = self.do_encrypt_aes_ecb(key)

            # import the key to aes
            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key.decode('latin-1'),
                aes_key_format=key_format,
                padding=pad_mode
            )
            admin.assert_success()

            # use aes to decrypt the data
            self.do_local_decrypt_aes_ecb(user, cipher_text)

    def import_aes_bin_encrypt_remote_test(
            self, rand_aes_key, akmd_akm
    ):
        """
        Steps:
            - Create an aes key locally
            - Import the key to akm
            - Use the imported key to encrypt some data
            - Use the local key to decrypt the data
        Expect:
            Data should decrypt correctly
        """
        with self.pcidss_off(akmd_akm):
            pad_mode = Padding.OAEP

            admin = akmd_akm.python_sdk_controller.akm_admin
            encr = akmd_akm.python_sdk_controller.akm_enc
            key_format = 'BIN'

            # create key
            key = rand_aes_key
            iv = os.urandom(16)

            # import the key to aes
            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key.decode('latin-1'),
                aes_key_format=key_format,
                padding=pad_mode
            )
            admin.assert_success()

            # use aes to encrypt some data
            resp = encr.aes_cbc_encrypt(
                self.plain_text.decode('latin-1'),
                self.aes_key_name,
                iv=iv.decode('latin-1'),
                padding_flag='N'
            )
            self.decrypt_aes_cbc(resp['Ciphertext'], key, iv)

    def import_aes_b16_any_pad_test(
            self, pad_mode, akmd_akm, rand_aes_key
    ):
        """
        Steps:
            - Create an AES key and encrypt something
            - Import key in B16 format with each padding flag
            - Retrieve the key, decode it, and use it to decrypt the thing

        Expect:
            Thing should decrypt
        """
        with self.pcidss_off(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin
            user = akmd_akm.python_sdk_controller.akm_client
            key_format = 'B16'

            # create key
            key = rand_aes_key

            # encrypt data with the key
            cipher_text = self.do_encrypt_aes_ecb(key)

            # b16 encode the key
            key = base64.b16encode(key).decode('utf-8')

            # import the key to aes
            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key,
                aes_key_format=key_format,
                padding=pad_mode
            )
            admin.assert_success()

            # use aes to decrypt the data
            self.do_local_decrypt_aes_ecb(user, cipher_text)

    def import_aes_b64_any_pad_test(
            self, pad_mode, akmd_akm, rand_aes_key
    ):
        """
        Steps:
            - Create an AES key and encrypt something
            - Encode the key in B64
            - Import key in B64 format with each padding flag
            - Retrieve the key, decode it, and use it to decrypt the thing

        Expect:
            Thing should decrypt
        """
        with self.pcidss_off(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin
            user = akmd_akm.python_sdk_controller.akm_client
            key_format = 'B64'

            # create key
            key = rand_aes_key

            # encrypt data with the key
            cipher_text = self.do_encrypt_aes_ecb(key)

            # b64 encode the key
            key = base64.b64encode(key).decode('utf-8')

            # import the key to aes
            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key,
                aes_key_format=key_format,
                padding=pad_mode
            )
            admin.assert_success()

            # use aes to decrypt the data
            self.do_local_decrypt_aes_ecb(user, cipher_text)

    def pcidss_yes_bin_import_test(self, akmd_akm, rand_aes_key):
        """
        Steps:
            - Set the PCIDSS flag to yes
            - Try to import an AES key in binary format
        Expect:
            Key should fail to import.
        """
        with self.pcidss_on(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin
            key_format = 'BIN'
            pad_mode = '0'
            key = rand_aes_key

            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key,
                aes_key_format=key_format,
                padding=pad_mode
            )
            assert admin.err() == Status.PCIDSS_FORBIDDEN

    def pcidss_yes_b16_import_test(self, akmd_akm, rand_aes_key):
        """
        Steps:
            - Set the PCIDSS flag to yes
            - Try to import an AES key in B16 format
        Expect:
            Key should fail to import.
        """
        with self.pcidss_on(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin
            key_format = 'B16'
            pad_mode = '0'

            key = base64.b16encode(rand_aes_key).decode('utf-8')

            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key,
                aes_key_format=key_format,
                padding=pad_mode
            )
            assert admin.err() == Status.PCIDSS_FORBIDDEN

    def pcidss_yes_b64_import_test(self, akmd_akm, rand_aes_key):
        """
        Steps:
            - Set the PCIDSS flag to yes
            - Try to import an AES key in B64 format
        Expect:
            Key should fail to import.
        """
        with self.pcidss_on(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin
            key_format = 'B64'
            pad_mode = '0'

            key = base64.b64encode(rand_aes_key).decode('utf-8')

            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=key,
                aes_key_format=key_format,
                padding=pad_mode
            )
            assert admin.err() == Status.PCIDSS_FORBIDDEN

    def pcidss_no_rsa_import_test(self, akmd_akm, rand_aes_key):
        """
        Steps:
            - Set the PCIDSS flag to no
            - Try to import an RSA wrapped AES key
        Expect:
            Key should import successfully.
        """
        with self.pcidss_off(akmd_akm):
            admin = akmd_akm.python_sdk_controller.akm_admin

            # generate rsa key
            private_key = rsa.generate_private_key(
                public_exponent=65537,
                key_size=2048,
                backend=default_backend()
            )

            # import rsa key
            priv_key_str = private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
            ).decode('latin-1')
            admin.import_wrapper_private_key(
                priv_key_str, self.wrapper_key_name
            )

            # create aes key
            key = rand_aes_key

            # encrypt aes key
            encrypted_aes_key = private_key.public_key().encrypt(
                key,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA1()),
                    algorithm=hashes.SHA1(),
                    label=None
                )
            )

            # import aes key
            admin.import_aes_key(
                self.aes_key_name,
                '0256',
                aes_key_value=encrypted_aes_key.decode('latin-1'),
                aes_key_format='RSA',
                wrapper_private_key_name=self.wrapper_key_name,
                padding=Padding.OAEP
            )
            assert admin.err() == Status.SUCCESS
