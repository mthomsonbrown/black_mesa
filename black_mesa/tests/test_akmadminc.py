"""
Tests to exercise the akmadmin.c client application.

State:
    These are not yet exhaustive.  The first tests implemented are to validate
    bugs that have been reported by customers or found through manual testing.
"""
# pylint: disable=too-many-lines

from pathlib import Path
import time
import pytest

from akm_sdk.akm import Padding, Access

from black_mesa.fixtures.akmadminc import AKMAdminC
from black_mesa.akmd_status import AKMDStatus
from black_mesa.akmadminc_status import AKMAdminCStatus
from black_mesa.logger import Logger


LOG = Logger(__name__).logger


@pytest.mark.akmadminc
class TestAKMAdminC(AKMAdminC):

    def test_get_key_access_list_no_keys(self, clients):
        """
        Try to get key access list with no keys.

        Expect:
            Status returned is NO_KEYS_FOUND
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute('--get-key-access-list')
        assert res['msg']['Return code'] == AKMDStatus.NO_KEYS_FOUND

    def test_get_key_access_list_one_skey(self, clients):
        """
        Try to get key access list with an skey in the database.

        Expect:
            Status returned is SUCCESS
        """
        psc, aac = clients
        psc.akm_admin.create_aes_key('bob')
        res = aac.execute('--get-key-access-list')
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_get_certificate_list(self, clients):
        """Validate Bug: AKMC-9"""
        _, aac = clients
        cert_name = 'admincert7'
        res = aac.execute((
            '--import-certificate '
            '--certificate-name {} '
            '--certificate-type A  --overwrite-flag N '
            '--file-name {}'
            ).format(
                cert_name,
                aac.remote_cert
            ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        res = aac.execute('--get-certificate-list --certificate-type A')
        assert cert_name in res['msg']['Certificate name']

    def test_import_private_key(self, clients):
        """Validate Bug: AKMC-11"""
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--import-private-key '
            '--private-key-name QKMIEPkeyJ10Y2BCE2VW477 '
            '--overwrite-flag N '
            '--file-name {}'
            ).format(
                aac.remote_key
            ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        res = aac.execute('--get-private-key-list')
        assert 'QKMIEPkeyJ10Y2BCE2VW477' in res['msg']['Private key']

    def test_create_sym_key_rollover_invalid(self, clients):
        """
        Validate Bug: AKMC-2

        Invalid argument to rollover should not produce errant j's at the
        beginning of the error message.
        """
        _, aac = clients
        res = aac.execute((
            '--create-sym-key '
            '--key-name QTM_RESERVED '
            '--key-size-bits 0256 '
            '--activation-date 00000000 '
            '--expiration-date 00000000 '
            '--rollover M '
            '--rollover-days 0000 '
            '--deletable Y '
            '--mirror N '
            '--key-access 1 '
            '--user-name User1 '
            '--group-name Group1'
        ))
        for _, value in res['err'].items():
            for item in value:
                assert not item.startswith('jjj ')

    def test_metadata_success(self, clients):
        """
        Validate Bug: AKMC-3

        Using the metadata flag should not cause a TLSRead error
        """
        psc, aac = clients
        keyname = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(keyname)
        res = aac.execute((
            '--set-metadata '
            '--key-name {} '
            '--instance " " '
            '--md01 abc '
            '--md02 abc '
            '--md03 abc '
            '--md04 abc '
            '--md05 abc '
            '--md06 abc '
            '--md07 abc '
            '--md08 abc '
            '--md09 abc '
            '--md10 abc '
            '--md11 abc '
            '--md12 abc '
            '--md13 abc '
            '--md14 abc '
            '--md15 abc '
            '--md16 abc '
        ).format(keyname))
        assert not res['err']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_export_certificate_handles_filename(self, clients):
        """
        Validate Bug: AKMC-18
        """
        psc, aac = clients
        cert_name = 'admincert7'
        export_file = '/tmp/afile.pem'
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--import-certificate '
            '--certificate-name {} '
            '--certificate-type C --overwrite-flag N '
            '--file-name {}'
            ).format(
                cert_name,
                aac.remote_cert
            ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        res = aac.execute((
            '--export-certificate '
            '--certificate-type C '
            '--certificate-name {} '
            '--file-name {}'
        ).format(
            cert_name,
            export_file
        ))

        assert not res['err']
        orig_cert = aac.remote.exec_check('cat {}'.format(export_file)).decode('utf-8')
        ex_cert = aac.remote.exec_check('cat {}'.format(aac.remote_cert)).decode('utf-8')
        cert_begin = '-----BEGIN CERTIFICATE-----\n'
        cert_end = '-----END CERTIFICATE-----\n'
        orig_cert_val = orig_cert[
            orig_cert.index(cert_begin):orig_cert.index(cert_end)
        ]
        ex_cert_val = ex_cert[
            ex_cert.index(cert_begin):ex_cert.index(cert_end)
        ]
        assert orig_cert_val == ex_cert_val

    def test_display_key_name_list_has_only_one_return_code(self, clients):
        """
        Validate Bug: AKMC-21
        """
        psc, aac = clients
        admin = psc.akm_admin
        admin.clean_db()
        for num in range(5):
            admin.create_aes_key("key{}".format(num))

        res = aac.execute('--display-key-name-list')
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_activate_key_does_not_segfault(self, clients):
        """
        Validate Bug: AKMC-28
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        res = aac.execute((
            '--activate-key-instance --key-name {}'
            ' --instance " " --deletable Y'
        ).format(key_name))
        assert len(res['err']) == 1
        assert AKMAdminCStatus.INVALID_ACTIVATION_DATE in res['err']

    def test_get_template_depth_returns_correct_error(self, clients):
        """
        Validate Bug: AKMC-30
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--get-template-depth --constant test '
            '--increment-len 5 --constant-len 4'
        ))
        assert not res['err']
        assert res['msg']['Return code'] == AKMDStatus.GET_TEMPLATE_DEPTH_ERROR

    def test_display_rsa_key_policy_private_instance_only(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        user = psc.akm_client
        key_data = user.get_rsa_private_key(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --instance {} --rsa-key-type Priv'
        ).format(key_data['Instance']))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_set_user_access_to_rsa_key(self, clients):
        psc, _ = clients
        key_name = 'bob'
        psc.akm_admin.set_log_level(50)
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        time.sleep(2)
        psc.akm_admin.set_user_access_to_key(key_name, 'user')
        # aac.execute((
        #     '--set-user-access-to-key --key-name {} --user-name User'.format(key_name)
        # ))
        assert psc.akm_admin.err() == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_public_instance_only(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        user = psc.akm_client
        key_data = user.get_rsa_public_key(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --instance {} --rsa-key-type Pub'
        ).format(key_data['Instance']))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_public_name_only(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --key-name {} --rsa-key-type Pub'
        ).format(key_name))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_private_name_only(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --key-name {} --rsa-key-type Priv'
        ).format(key_name))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_public_name_and_instance(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        user = psc.akm_client
        key_data = user.get_rsa_public_key(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --key-name {} --instance {} '
            '--rsa-key-type Pub'
        ).format(key_name, key_data['Instance']))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_private_name_and_instance(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        user = psc.akm_client
        key_data = user.get_rsa_private_key(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --key-name {} --instance {} '
            '--rsa-key-type Priv'
        ).format(key_name, key_data['Instance']))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_display_rsa_key_policy_name_and_instance_mismatch(self, clients):
        """
        Validate Bug: AKMC-40
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)
        user = psc.akm_client
        key_data = user.get_rsa_private_key(key_name)

        res = aac.execute((
            '--display-rsa-key-policy --key-name {} --instance {} '
            '--rsa-key-type Priv'
        ).format('not{}'.format(key_name), key_data['Instance']))
        assert res['msg']['Return code'] == AKMDStatus.RSA_KEY_NOT_FOUND

    def test_export_rsa_private_key_disabled(self, clients):
        """
        Validate Bug: AKMC-43
        """
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)

        res = aac.execute((
            '--export-rsa-private-key --key-name {}'
        ).format(key_name))
        assert len(res['err']) == 1
        print("RED IS {}".format(res))
        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

    def test_export_rsa_public_key_returns_value(self, clients):
        """
        Validate Bug: AKMC-42
        """
        psc, aac = clients
        key_name = 'bob'
        export_file = Path('/home/admin/downloads/export_key')
        psc.akm_admin.clean_db()
        psc.akm_admin.set_log_level(50)
        psc.akm_admin.create_rsa_key_pair(key_name)
        key_data = psc.akm_client.get_rsa_public_key(key_name)
        res = aac.execute((
            '--export-rsa-public-key --key-name {key_name} '
            '--instance {instance} --file-name {efp}'
        ).format(
            key_name=key_name,
            instance=key_data['Instance'],
            efp=export_file
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        orig_key = key_data['KeyValue']

        ex_key = aac.remote.exec_check('cat {}'.format(export_file))
        assert ex_key == orig_key

    def test_import_sym_key_shows_return_code(self, clients):
        """
        Validate Bug: AKMC-14
        """
        psc, aac = clients
        key_name = 'bob'
        export_file = Path('/home/admin/downloads/export_key')
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        key_data = psc.akm_client.get_aes_key(key_name)
        res = aac.execute((
            '--export-sym-key --key-name {key_name} '
            '--instance {instance} --file-name {efp} '
            '--key-format B64 --certificate-name "" --rsa-padding-mode {pad}'
        ).format(
            key_name=key_name,
            instance=key_data['Instance'],
            efp=export_file,
            pad=Padding.OAEP
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_admin.clean_db()
        res = aac.execute((
            '--import-sym-key '
            '--key-name {key_name} '
            '--activation-date 00000000 '
            '--expiration-date 00000000 '
            '--rollover-code M '
            '--rollover-days 0000 '
            '--deletable Y '
            '--mirror-flag N '
            '--key-access {access} '
            '--user-name "" '
            '--group-name "" '
            '--key-format B64 '
            '--private-key-name "" '
            '--rsa-padding-mode {pad} '
            '--file-name {key_file} '
            '--key-size-bits 0128'
        ).format(
            key_name=key_name,
            access=Access.All,
            pad=Padding.OAEP,
            key_file=export_file
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_import_sym_key_pads_key_size(self, clients):
        """
        Validate Bug: AKMC-48
        """
        psc, aac = clients
        key_name = 'bob'
        export_file = Path('/home/admin/downloads/export_key')
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        key_data = psc.akm_client.get_aes_key(key_name)
        res = aac.execute((
            '--export-sym-key --key-name {key_name} '
            '--instance {instance} --file-name {efp} '
            '--key-format B64 --certificate-name "" --rsa-padding-mode {pad}'
        ).format(
            key_name=key_name,
            instance=key_data['Instance'],
            efp=export_file,
            pad=Padding.OAEP
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_admin.clean_db()
        res = aac.execute((
            '--import-sym-key '
            '--key-name {key_name} '
            '--activation-date 00000000 '
            '--expiration-date 00000000 '
            '--rollover-code M '
            '--rollover-days 0000 '
            '--deletable Y '
            '--mirror-flag N '
            '--key-access {access} '
            '--user-name "" '
            '--group-name "" '
            '--key-format B64 '
            '--private-key-name "" '
            '--rsa-padding-mode {pad} '
            '--file-name {key_file} '
            '--key-size-bits 128'
        ).format(
            key_name=key_name,
            access=Access.All,
            pad=Padding.OAEP,
            key_file=export_file
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_import_rsa_priv_takes_filename(self, clients):
        """
        Validate Bug: AKMC-39
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        downloads = Path('/home/admin/downloads/')
        priv_key_name = 'private.der'
        key_name = 'bob'

        aac.remote.exec_check((
            'openssl genpkey -algorithm RSA -out {} '
            '-outform DER -pkeyopt rsa_keygen_bits:2048'
        ).format(downloads / priv_key_name), ignore='...')

        res = aac.execute((
            '--import-rsa-private-key --key-name {key_name} '
            '--rsa-key-size-bits 02048 '
            '--activation-date 00000000 '
            '--expiration-date 00000000 '
            '--deletable Y '
            '--mirror-flag N '
            '--key-access {access} '
            '--user-name "" '
            '--group-name "" '
            '--file-name {key_file} '
        ).format(
            key_name=key_name,
            access=Access.All,
            key_file=downloads / priv_key_name
        ))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        key_data = psc.akm_client.get_rsa_private_key(key_name)
        assert key_data['KeyName'] == key_name

    def test_import_rsa_pub_takes_filename(self, clients):
        """
        Validate Bug: AKMC-39
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        downloads = Path('/home/admin/downloads/')
        priv_key_name = 'private.pem'
        pub_key_name = 'public.der'
        key_name = 'bob'

        aac.remote.exec_check((
            'openssl genpkey -algorithm RSA -out {} '
            '-outform DER -pkeyopt rsa_keygen_bits:2048'
        ).format(downloads / priv_key_name), ignore='...')

        aac.remote.exec_check((
            'openssl rsa -inform DER -in {infile} '
            '-outform DER -RSAPublicKey_out -out {outfile}'
        ).format(
            infile=downloads / priv_key_name,
            outfile=downloads / pub_key_name
        ), ignore="writing RSA key")

        res = aac.execute((
            '--import-rsa-public-key --key-name {key_name} '
            '--rsa-key-size-bits 02048 '
            '--activation-date 00000000 '
            '--expiration-date 00000000 '
            '--deletable Y '
            '--mirror-flag N '
            '--key-access {access} '
            '--user-name "" '
            '--group-name "" '
            '--file-name {key_file} '
        ).format(
            key_name=key_name,
            access=Access.All,
            key_file=downloads / pub_key_name
        ))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        key_data = psc.akm_client.get_rsa_public_key(key_name)
        assert key_data['KeyName'] == key_name

    def test_change_public_rsa_expiration_date(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-public --key-name {} '
            '--expiration-date 00000000'
        ).format(key_name))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_change_private_rsa_expiration_date(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-private --key-name {} '
            '--expiration-date 00000000'
        ).format(key_name))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_change_private_rsa_expiration_date_no_expiration(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-private --key-name {}'
        ).format(key_name))

        assert res['msg']['Invalid number of options']['expected'] == '4'
        assert res['msg']['Invalid number of options']['given'] == '2'

    def test_change_public_rsa_expiration_date_no_expiration(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-public --key-name {}'
        ).format(key_name))

        assert res['msg']['Invalid number of options']['expected'] == '4'
        assert res['msg']['Invalid number of options']['given'] == '2'

    def test_change_public_rsa_expiration_date_no_name(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-public '
            '--expiration-date 00000000'
        ))

        assert res['msg']['Invalid number of options']['expected'] == '4'
        assert res['msg']['Invalid number of options']['given'] == '2'

    def test_change_private_rsa_expiration_date_no_name(self, clients):
        """
        Validate Bug: AKMC-38
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name)
        res = aac.execute((
            '--change-expiration-date-private '
            '--expiration-date 00000000'
        ))

        assert res['msg']['Invalid number of options']['expected'] == '4'
        assert res['msg']['Invalid number of options']['given'] == '2'

    def test_change_deletable_public_rsa(self, clients):
        """
        Validate Bug: AKMC-37
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name, deletable='Y')
        res = aac.execute((
            '--change-deletable-flag-public --key-name {} '
            '--deletable-flag n'
        ).format(key_name))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_admin.delete_rsa_key(key_name)
        assert psc.akm_admin.err() == AKMDStatus.RSA_NOT_DELETABLE

    def test_change_deletable_private_rsa(self, clients):
        """
        Validate Bug: AKMC-37
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name, deletable='Y')
        res = aac.execute((
            '--change-deletable-flag-private --key-name {} '
            '--deletable-flag n'
        ).format(key_name))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_admin.delete_rsa_key(key_name)
        assert psc.akm_admin.err() == AKMDStatus.RSA_NOT_DELETABLE

    def test_change_deletable_public_rsa_no_value_for_flag(self, clients):
        """
        Validate Bug: AKMC-37
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name, deletable='Y')
        res = aac.execute((
            '--change-deletable-flag-public --key-name {} '
            '--deletable-flag'
        ).format(key_name))

        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

        psc.akm_admin.delete_rsa_key(key_name)
        assert psc.akm_admin.err() == AKMDStatus.SUCCESS

    def test_change_deletable_private_rsa_no_value_for_flag(self, clients):
        """
        Validate Bug: AKMC-37
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        key_name = 'bob'

        psc.akm_admin.create_rsa_key_pair(key_name, deletable='Y')
        res = aac.execute((
            '--change-deletable-flag-private --key-name {} '
            '--deletable-flag'
        ).format(key_name))

        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

        psc.akm_admin.delete_rsa_key(key_name)
        assert psc.akm_admin.err() == AKMDStatus.SUCCESS

    def test_trigger_put_failure(self, clients):
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--trigger-put '
            '--mirror-name bob'
        ))

        assert res['msg']['Return Code'] == AKMDStatus.NO_ENTRY_FOR_MIRROR_NAME

    def test_trigger_put_no_mirror_value(self, clients):
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--trigger-put '
            '--mirror-name'
        ))

        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

    def test_trigger_put_no_options(self, clients):
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--trigger-put'
        ))

        assert 'Invalid number of options' in res['msg']

    def test_trigger_put_functional(self, mirror_systems):
        prime_akm, sec_akm = mirror_systems
        key_name = 'bob'
        p_admin = prime_akm.python_sdk_controller.akm_admin
        s_client = sec_akm.python_sdk_controller.akm_client

        # turn off secondary
        sec_akm.turn_off_akmd()
        # create key on primary
        p_admin.create_aes_key(key_name)
        # turn on secondary
        sec_akm.turn_on_akmd()
        # assert no key on secondary
        self.psc_wait(
            s_client.get_aes_key,
            s_client.err,
            key_name,
            state=AKMDStatus.NO_ENTRY_FOR_KEY_NAME_INSTANCE
        )
        # call trigger put on primary
        prime_akm.akmadminc_controller.execute(
            '--trigger-put --mirror-name {}'.format(sec_akm.name)
        )
        # assert key on secondary
        self.psc_wait(s_client.get_aes_key, s_client.err, key_name)

    def test_no_error_with_log_level_zero(self, clients):
        """
        Validate Bug: AKMC-45
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--set-log-level --log-level 0'
        ))

        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_error_with_log_level_negative(self, clients):
        """
        Validate Bug: AKMC-45
        """
        psc, aac = clients
        psc.akm_admin.clean_db()
        res = aac.execute((
            '--set-log-level --log-level -7'
        ))

        assert res['msg']['Return code'] == AKMDStatus.INVALID_CHARACTER

    def test_validate_db(self, clients):
        _, aac = clients
        res = aac.execute('--validate-db')
        assert res['msg']['Return Code'] == AKMDStatus.SUCCESS

    def test_validate_db_no_db(self, akm):
        akm.hide_database()
        res = akm.akmadminc_controller.execute('--validate-db')
        assert res['msg']['Return Code'] == AKMDStatus.CANNOT_OPEN_DATABASE
        akm.unhide_database()
        res = akm.akmadminc_controller.execute('--validate-db')
        assert res['msg']['Return Code'] == AKMDStatus.SUCCESS

    def test_force_key_sync_by_name(self, mirror_systems):
        prime_akm, sec_akm = mirror_systems
        key_name = 'bob'
        p_admin = prime_akm.python_sdk_controller.akm_admin
        p_client = prime_akm.python_sdk_controller.akm_client
        s_client = sec_akm.python_sdk_controller.akm_client

        # clear mirror address from primary
        p_admin.clean_db()
        # create key on primary
        p_admin.create_aes_key(key_name)
        # assert key on primary
        self.psc_wait(
            p_client.get_aes_key,
            p_client.err,
            key_name,
            state=AKMDStatus.SUCCESS
        )
        # Set secondary as mirror
        p_admin.set_mirror_address(sec_akm.name, sec_akm.ip, '6002')
        # assert no key on secondary
        self.psc_wait(
            s_client.get_aes_key,
            s_client.err,
            key_name,
            state=AKMDStatus.NO_ENTRY_FOR_KEY_NAME_INSTANCE
        )
        # call force key sync on primary
        res = prime_akm.akmadminc_controller.execute(
            '--force-key-sync --mirror-name {} '
            '--key-name bob'.format(sec_akm.name)
        )
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        # assert key on secondary
        self.psc_wait(s_client.get_aes_key, s_client.err, key_name)

    def test_force_key_sync_all(self, mirror_systems):
        prime_akm, sec_akm = mirror_systems
        key_name = 'bob'
        p_admin = prime_akm.python_sdk_controller.akm_admin
        p_client = prime_akm.python_sdk_controller.akm_client
        s_client = sec_akm.python_sdk_controller.akm_client

        # clear mirror address from primary
        p_admin.clean_db()
        # create key on primary
        p_admin.create_aes_key(key_name)
        # assert key on primary
        self.psc_wait(
            p_client.get_aes_key,
            p_client.err,
            key_name,
            state=AKMDStatus.SUCCESS
        )
        # set secondary as mirror
        p_admin.set_mirror_address(sec_akm.name, sec_akm.ip, '6002')
        # assert no key on secondary
        self.psc_wait(
            s_client.get_aes_key,
            s_client.err,
            key_name,
            state=AKMDStatus.NO_ENTRY_FOR_KEY_NAME_INSTANCE
        )
        # call force key sync on primary
        res = prime_akm.akmadminc_controller.execute((
            '--force-key-sync --all-keys Y --mirror-name {}'
        ).format(sec_akm.name))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        # assert key on secondary
        self.psc_wait(s_client.get_aes_key, s_client.err, key_name)

    @pytest.mark.reer
    def test_delete_ekm_mirrored(self, mirror_systems):
        prime_akm, sec_akm = mirror_systems
        key_name = 'bobby'
        p_admin = prime_akm.python_sdk_controller.akm_admin
        s_admin = sec_akm.python_sdk_controller.akm_admin

        p_admin.create_ekm_key(key_name, 2048)
        assert p_admin.err() == AKMDStatus.SUCCESS

        polling_frequency = 0.5
        time_stop = time.time() + 5
        while time.time() < time_stop:
            keys = s_admin.display_ekm_key_list()
            if key_name in keys:
                break
            time.sleep(polling_frequency)
        assert key_name in keys

        p_admin.delete_ekm_key(key_name)
        assert p_admin.err() == AKMDStatus.SUCCESS

        time_stop = time.time() + 5
        while time.time() < time_stop:
            keys = s_admin.display_ekm_key_list()
            if not keys:
                break
            time.sleep(polling_frequency)
        assert not keys

    def test_force_rollover(self, clients):
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        b_dat = psc.akm_client.get_aes_key(key_name)
        res = aac.execute('--force-rollover')
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        a_dat = psc.akm_client.get_aes_key(key_name)
        assert b_dat['KeyValue'] != a_dat['KeyValue']

    def test_revoke_key_instance(self, clients):
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        b_dat = psc.akm_client.get_aes_key(key_name)
        aac.execute('--force-rollover')
        psc.akm_client.get_aes_key(key_name, instance=b_dat['Instance'])
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        aac.execute((
            '--revoke-key-instance --instance {} --key-name {}'
        ).format(b_dat['Instance'], key_name))
        psc.akm_client.get_aes_key(key_name, instance=b_dat['Instance'])
        assert psc.akm_client.err() == AKMDStatus.KEY_HAS_BEEN_REVOKED

    def test_revoke_key_instance_too_many_args(self, clients):
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        b_dat = psc.akm_client.get_aes_key(key_name)
        aac.execute('--force-rollover')
        psc.akm_client.get_aes_key(key_name, instance=b_dat['Instance'])
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--revoke-key-instance --instance {} --key-name {} --an-arg thing'
        ).format(b_dat['Instance'], key_name))
        assert AKMAdminCStatus.INVALID_ARGUMENT in res['err']

    def test_revoke_key_instance_missing_value(self, clients):
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        b_dat = psc.akm_client.get_aes_key(key_name)
        psc.akm_admin.force_rollover()
        psc.akm_client.get_aes_key(key_name, instance=b_dat['Instance'])
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--revoke-key-instance --instance {} --key-name'
        ).format(b_dat['Instance']))
        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

    def test_remove_group_access_to_key(self, clients):
        """TODO: Fix hard-coded group name"""
        psc, aac = clients
        group_name = 'group'
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name, access=Access.Group, group_name=group_name
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-group-access-to-key --key-name {} --group-name {} '
            '--ignore-missing-record N'
        ).format(
            key_name, group_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_GROUP_ACCESS_ERROR

    def test_remove_group_access_to_key_no_group_name(self, clients):
        """TODO: Fix hard-coded group name"""
        psc, aac = clients
        group_name = 'group'
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name, access=Access.Group, group_name=group_name
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-group-access-to-key --key-name {} '
            '--ignore-missing-record N --group-name'
        ).format(key_name))
        assert AKMAdminCStatus.INVALID_COMMAND_VALUE in res['err']

    def test_remove_key_from_key_access(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-key-from-key-access --key-name {} '
            '--ignore-missing-record N'
        ).format(
            key_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_KEY_ACCESS_ERROR

    def test_remove_template_record(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        psc.akm_admin.clean_db()
        psc.akm_admin.auto_gen_aes_keys(key_name)
        psc.akm_admin.auto_gen_aes_keys(key_name, mode='A')
        assert psc.akm_admin.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-template-record --constant {} --ignore-missing-record N '
            '--constant-len {} --increment-len {}'
        ).format(
            key_name, len(key_name), '01'
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_admin.auto_gen_aes_keys(key_name, mode='A')
        assert psc.akm_admin.err() == AKMDStatus.AUTO_GEN_KEYS_ERROR

    def test_remove_user_access_to_key(self, clients):
        """TODO: Fix hard coded user name"""
        psc, aac = clients
        key_name = 'suzie'
        user_name = 'user'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name, access=Access.User, user_name=user_name
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-user-access-to-key --key-name {} --user-name {} '
            '--ignore-missing-record N'
        ).format(
            key_name, user_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_USER_ACCESS_ERROR

    def test_remove_user_from_group(self, clients):
        """TODO: Fix hard coded user and group name"""
        psc, aac = clients
        key_name = 'suzie'
        user_name = 'user'
        group_name = 'group'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name,
            access=Access.UserGroupStrict,
            group_name=group_name,
            user_name=user_name
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_GROUP_MEMBER_ERROR
        psc.akm_admin.add_user_to_group(group_name, user_name)
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS
        res = aac.execute((
            '--remove-user-from-group --group-name {} --user-name {} '
            '--ignore-missing-record N'
        ).format(
            group_name, user_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_GROUP_MEMBER_ERROR

    def test_set_group_access_to_key(self, clients):
        """TODO: Fix hard coded group name"""
        psc, aac = clients
        key_name = 'suzie'
        group_name = 'group'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name, access=Access.Group, group_name=group_name + 'not'
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_GROUP_ACCESS_ERROR
        res = aac.execute((
            '--set-group-access-to-key --group-name {} --key-name {}'
        ).format(
            group_name, key_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS

    def test_set_key_access_flag(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        group_name = 'group'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name,
            access=Access.UserGroup,
            user_name='fakename',
            group_name=group_name
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_USER_ACCESS_ERROR
        res = aac.execute((
            '--set-key-access-flag --key-name {} --key-access {}'
        ).format(
            key_name, Access.Group
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS

    def test_set_user_access_to_key(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        user_name = 'user'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(
            key_name,
            access=Access.User,
            user_name='fakename'
        )
        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.READ_USER_ACCESS_ERROR
        res = aac.execute((
            '--set-user-access-to-key --key-name {} --user-name {}'
        ).format(
            key_name, user_name
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

        psc.akm_client.get_aes_key(key_name)
        assert psc.akm_client.err() == AKMDStatus.SUCCESS

    def test_retrieve_two_metadata(self, clients):
        psc, aac = clients
        key_name_3 = 'EKA00E09E3EAB2800000'
        psc.akm_admin.clean_db()
        key_names = ['{}{}'.format(key_name_3, i) for i in range(30)]
        for name in key_names:
            psc.akm_admin.create_aes_key(name)
        command = (
            '--set-metadata '
            '--key-name {} '
            '--instance " " '
            '--md01 "" '
            '--md02 "" '
            '--md03 "" '
            '--md04 "{}" '
            '--md05 "" '
            '--md06 "" '
            '--md07 "" '
            '--md08 "" '
            '--md09 "" '
            '--md10 "" '
            '--md11 "" '
            '--md12 "" '
            '--md13 "" '
            '--md14 "" '
            '--md15 "" '
            '--md16 "" '
        )
        for name in key_names:
            res = aac.execute(command.format(
                name, 'QKQTMF5C1623003500308'
            ))
            assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        res = aac.execute((
            '--retrieve-metadata --md04 --ct "F5"'
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_retrieve_metadata(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        res = aac.execute((
            '--set-metadata '
            '--key-name {} '
            '--instance " " '
            '--md01 abc '
            '--md02 def '
            '--md03 def '
            '--md04 def '
            '--md05 def '
            '--md06 def '
            '--md07 def '
            '--md08 def '
            '--md09 def '
            '--md10 def '
            '--md11 def '
            '--md12 def '
            '--md13 def '
            '--md14 def '
            '--md15 def '
            '--md16 def '
        ).format(key_name))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        res = aac.execute((
            '--retrieve-metadata --md01 --eq abc'
        ))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS

    def test_retrieve_metadata_not_found(self, clients):
        psc, aac = clients
        key_name = 'suzie'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_aes_key(key_name)
        res = aac.execute((
            '--set-metadata '
            '--key-name {} '
            '--instance " " '
            '--md01 abc '
            '--md02 def '
            '--md03 def '
            '--md04 def '
            '--md05 def '
            '--md06 def '
            '--md07 def '
            '--md08 def '
            '--md09 def '
            '--md10 def '
            '--md11 def '
            '--md12 def '
            '--md13 def '
            '--md14 def '
            '--md15 def '
            '--md16 def '
        ).format(key_name))
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
        res = aac.execute((
            '--retrieve-metadata --md02 --eq abc'
        ))
        assert res['msg']['Return code'] == AKMDStatus.QUERY_METADATA_ERROR

    def test_auth_admin(self, akm):
        window_minutes = 1
        key_name = 'kyle'
        psc = akm.python_sdk_controller
        try:
            akm.set_dual_knowledge('Y')
            psc.akm_admin.create_aes_key(key_name)
            assert psc.akm_admin.err() == AKMDStatus.AUTH_ADMIN_WINDOW_NOT_SET
            psc.akm_admin.auth_admin(window_minutes)
            akm.akmadminc_controller.execute((
                '--auth-admin --time 0001'
            ))
            psc.akm_admin2.create_aes_key(key_name)
            assert psc.akm_admin2.err() == AKMDStatus.SUCCESS
            psc.akm_admin.create_aes_key(key_name)
            assert psc.akm_admin.err() == AKMDStatus.AUTH_ADMIN_SAME_AS_REQUEST_ADMIN
            LOG.debug(f"Sleeping for {window_minutes} minutes")
            time.sleep(60 * window_minutes + 5)
            psc.akm_admin.create_aes_key(key_name)
            assert psc.akm_admin.err() == AKMDStatus.AUTH_ADMIN_SAME_AS_REQUEST_ADMIN
            psc.akm_admin2.create_aes_key(key_name)
            assert psc.akm_admin2.err() == AKMDStatus.AUTH_ADMIN_WINDOW_EXPIRED
        finally:
            akm.set_dual_knowledge('N')

    def test_create_ekm_key(self, clients):
        psc, aac = clients
        key_name = 'bob'
        psc.akm_admin.clean_db()
        psc.akm_admin.create_rsa_key_pair(key_name)

        psc.akm_admin.create_ekm_key('bob', '2048')
        psc.akm_admin.assert_success()
        psc.akm_admin.enable_ekm_key('bob')
        psc.akm_admin.assert_success()
        res = aac.execute((
            '--enable-ekm-key --key-name bob --ekm-supported-flag-id Y '
            '--ekm-volatile-flag-id Y --ekm-exportable-flag-id Y --ekm-importable-flag-id Y'
        ))

        res = aac.execute((
            '--create-ekm-key --key-name {} --ekm-key-size 2048'
        ).format(key_name))
        assert not res['err']
        assert key_name in res['msg']['Key name']
        assert res['msg']['Return code'] == AKMDStatus.SUCCESS
