"""
Programmatic interface to remote bootstrap API.
For api functions in bootstrap < 3.0
"""
import os

from black_mesa.environment import Environment
from black_mesa.logger import Logger


class LegacyBootstrapController(object):
    """
    Issue commands to the bootstrap 2.x API.
    """
    def __init__(self, remote, api_path):
        self.log = Logger(__name__).logger
        self.env = self.load_config()['bootstrap']
        self.api_path = api_path
        self.remote = remote
        self.name = None

    def bs_api(self, endpoint, arguments=None):
        """
        Send a request to the bootstrap api.

        Args:
            endpoint (str): The method to call.
            arguments (str): Arguments to pass to the method.
        """
        method = os.path.join(self.api_path, endpoint)
        self.remote.exec_check(
            'python {method} {args}'.format(method=method, args=arguments),
            verbose=True
        )

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'bootstrap': {
                'system_name': 'drogon',
                'ca': {
                    'country': 'US',
                    'state': 'Oregon',
                    'locale': 'Portland',
                    'organization': 'TestCo'
                },
                'license': None,
                'keys': False,
                'users': True
            }
        })
        return env.load_config()

    def migrate(self):
        self.log.debug("Performing migration")
        self.bs_api(
            os.path.join('initialize', 'from_backup.py')
        )

    def old_initialize(self, primary=True):
        self.log.debug("Performing initialization")
        args = [
            self.env['ca']['country'],
            self.env['ca']['state'],
            self.env['ca']['locale'],
            self.env['ca']['organization'],
            self.env['system_name'],
            '--keys', '--users'
        ]
        if primary:
            args.append('--is-primary')

        self.bs_api(
            os.path.join('initialize', 'main.py'),
            ' '.join(args))

    def deploy_bootstrap(self):
        self.log.debug("Deploying bootstrap")
        self.bs_api(
            os.path.join('update', 'deploy_files.py'), 'bootstrap')

    def reset(self):
        self.log.debug("Resetting AKM installation")
        self.bs_api(
            os.path.join('dangerous', 'reset.py'))

    def initialize(self, name):
        """
        Initialize AKM through bootstrap CLI.

        Args:
            name (str): The name to give this AKM.
        """
        self.log.debug("Initializing AKM...")
        self.name = name
        self.remote.exec_check(
            'akm-cli init --users --keys us oregon portland testco {}'.format(name)
        )

    def set_as_mirror(self, prime_name):
        """
        Set this akm to be a mirror.

        Args:
            prime_name (str): The name of the primary AKM.
        """
        self.log.debug(f"Setting {self.name} as mirror.")
        self.remote.exec_check(
            'akm-cli set_as_mirror {} > /dev/null'.format(prime_name)
        )

    def add_mirror(self, ip):
        """
        After running the set_as_mirror command on a mirror AKM, set this
        AKM to copy its keys to that mirror.

        Args:
            ip (str): The IP address used to reach the new mirror.
        """
        self.log.debug(f"Adding {ip} as mirror to {self.name}")
        self.remote.exec_check(
            'ssh-keyscan -H {} >> ~/.ssh/known_hosts'.format(ip), ignore='#'
        )
        self.remote.exec_check(
            'akm-cli add_mirror {}'.format(ip)
        )
