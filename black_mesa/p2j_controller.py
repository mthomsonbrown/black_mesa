import time
from threading import Thread


class P2JController:
    def __init__(self, remote):
        self.remote = remote
        self.stuff = None

    def start(self, filepath, err_f_path=None):
        """
        Listen to the logging process' stdout and stderr.

        Args:
            filepath (Path): The file to write the logs to.
        """
        self.stuff = self.remote.async_exec('akm-log')
        xout = Thread(target=self._poll_log, args=(self.stuff[1], filepath))
        xout.start()

        err_f_path = err_f_path or filepath
        eout = Thread(target=self._poll_log, args=(self.stuff[2], err_f_path))
        eout.start()

    def stop(self):
        """
        Terminate listening process.
        """
        time.sleep(1)  # wait for last log entries
        self.remote.close()

    def _poll_log(self, sout, filepath):
        line_buf = b''
        while not sout.channel.exit_status_ready():
            line_buf += sout.read(1)
            if line_buf.endswith(b'\n'):
                with open(filepath, 'a') as logf:
                    logf.writelines(line_buf.decode('utf-8'))
                line_buf = b''
