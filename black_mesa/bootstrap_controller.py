import json

from black_mesa.logger import Logger


class BootstrapController:
    def __init__(self, remote):
        self.log = Logger(__name__).logger
        self.remote = remote

    def cli(self, cmd):
        """
        Run a command against the CLI, then parse the output.

        Args:
            The command to execute.

        Returns:
            dict: The success value and the returned JSON message.
        """
        res_string = self.remote.exec_check(f'akm-cli {cmd}').decode('utf-8')
        self.log.info("Response: %s", res_string)
        return json.loads(res_string)
