from pathlib import Path
from typing import Dict, List
from subprocess import run, PIPE
import json


class TerraformController:

    """
    Wrapper for the terraform CLI.
    """

    def __init__(self, workspace: Path):
        """
        Create a terraform controller

        Args:
            workspace: The path to the terraform config directory.
        """
        self.workspace = workspace

    def init(self):
        """
        refer to https://terraform.io/docs/commands/init.html
        """
        run(
            'terraform init',
            shell=True,
            cwd=self.workspace,
            check=True
        )

    def apply(self):
        """
        refer to https://terraform.io/docs/commands/apply.html
        """
        run(
            'terraform apply -auto-approve',
            shell=True,
            cwd=self.workspace,
            check=True
        )

    def output(self):
        """
        refer to https://www.terraform.io/docs/commands/output.html

        Returns:
            dict: The key value pairs from output
        """
        process = run(
            'terraform output -json',
            shell=True,
            cwd=self.workspace,
            stdout=PIPE,
            check=True
        )
        return json.loads(process.stdout.decode('utf-8'))

    def show(self):
        """
        refer to https://www.terraform.io/docs/commands/show.html

        Returns:
            dict: A list of all the resources provisioned
        """
        process = run(
            'terraform show -json',
            shell=True,
            cwd=self.workspace,
            stdout=PIPE,
            check=True
        )
        return json.loads(process.stdout.decode('utf-8'))

    def get_node_ips(self) -> Dict[str, List[str]]:
        """
        Parse node ips out of terraform output.

        Returns:
            The node ips and associated names and roles
        """
        nodes = {'masters': [], 'workers': []}

        terra_info = self.show()
        resource_list = terra_info['values']['root_module']['resources']

        for system in resource_list:
            if system['type'] != "aws_instance":
                continue

            pub_ip = system['values']['public_ip']
            name = system['values']['tags']['Name']

            if name == 'master-node':
                nodes['masters'].append(pub_ip)
                continue
            nodes['workers'].append(pub_ip)

        return nodes

    def get_ebs_id(self):
        """
        Return the id of the ebs volume created for persistent storage.
        """
        vol_ids = []
        terra_info = self.show()
        resource_list = terra_info['values']['root_module']['resources']

        for system in resource_list:
            if system['type'] != "aws_ebs_volume":
                continue

            vol_ids.append(system['values']['id'])

        if len(vol_ids) != 1:
            raise AssertionError("Number of EBS volumes wasn't exactly one")

        return vol_ids[0]

    def get_node_creds(self) -> (Path, str):
        """
        Discover the local key path and username to access nodes.

        Returns:
            The key path and username
        """
        data = self.output()
        return (
            Path(data['private_key_path']['value']).resolve(),
            data['user']['value']
        )

    def destroy(self):
        """
        refer to https://www.terraform.io/docs/commands/destroy.html
        """
        run(
            'terraform destroy -auto-approve',
            shell=True,
            cwd=self.workspace,
            check=True
        )
