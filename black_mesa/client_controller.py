import os
import shutil
import zipfile
import subprocess
import shlex
import re
from pathlib import Path

from black_mesa.remote_controller import RemoteController
from black_mesa.environment import Environment
from black_mesa.logger import Logger


class ClientController(object):
    """Base class for client side tool tooling."""
    def __init__(self, ip, username, key=None, password=None):
        self.log = Logger(__name__).logger
        self.env = self.load_config()
        self.workbench = self.env['workbench']
        self.env = self.env['akm_client']

        self.akm_ip = ip
        self.username = username
        self.key = key

        self.remote = RemoteController(ip, username, key=key, password=password)
        self.akm_name = self.remote.name
        self.local_storage = Path(
            self.workbench, self.env['local_storage'], self.akm_ip
        )

        # TODO: Pull this info from the PKI
        self.admin_users = ['admin1', 'admin2']
        self.client_users = ['user']
        self.enc_users = ['enc_user']

    @staticmethod
    def load_config():
        env = Environment()
        env.config_json.update({
            'akm_client': {
                'local_storage': 'local_pki',
                'downloads_dir': os.path.join(os.sep, 'home',
                                              'admin', 'downloads'),
                'admin_port': '6001',
                'client_port': '6000'
            }
        })
        return env.load_config()

    def download_pki(self, overwrite: bool = False, path: Path = None):
        """
        Pull all PKI archives from the AKM.

        Args:
            overwrite: If true, overwrite the local pki if it exists.
            path: The path to the pki on the remote system.
        """
        if self.local_storage.exists():
            if not overwrite:
                return
            shutil.rmtree(self.local_storage)
        self.local_storage.mkdir(parents=True)

        target_dir = path or self.env['downloads_dir']
        print(f"Downloading PKI from: {target_dir}")
        for target in self.remote.list_dir(target_dir):
            if '.zip' in target and 'ServerCertificates' not in target:
                self._create_pki_group(target, target_dir)

    def _create_pki_group(self, zip_name, dl_dir):
        """
        Pull a PKI zip from the AKM and create a local directory.

        Args:
            zip_name (str): Name of the zipfile on the remote system.
        """
        self.log.info(f"Downloading {zip_name}")
        dest = Path(self.local_storage, zip_name)
        self.remote.download(
            Path(dl_dir or self.env['downloads_dir'], zip_name),
            dest
        )

        self.log.info(f"Unzipping {zip_name}")
        zip_file = zipfile.ZipFile(dest, 'r')
        zip_file.extractall(str(self.local_storage))

    def iter_clients(self, pki_loc=None):
        """
        Iterate over a folder containing client PKI.

        Args:
            pki_loc (Path): Path to folder containing client PKI collections.

        Yields:
            Path: Location of a client PKI collection.
        """
        pki_loc = pki_loc or self.local_storage
        for client in pki_loc.iterdir():
            if client.is_dir():
                yield client

    def find_cert_key_ca(self, home):
        """
        Find cert, key, and ca in given PKI group.

        Args:
            home (Path): Path to the PKI folder.

        Returns:
            Path: Path to the cert
            Path: Path to the key
            Path: Path to the CA
        """
        cert = None
        key = None
        ca_cert = None
        for file_ in Path(self.local_storage, home).iterdir():
            if file_.name in ('AKMAdminCertificate.pem',
                              'AKMClientCertificate.pem'):
                cert = Path(home, file_)
            if file_.name in ('AKMAdminPrivateKey.pem',
                              'AKMClientPrivateKey.pem'):
                key = Path(home, file_)
            if file_.name == 'AKMRootCACertificate.pem':
                ca_cert = Path(home, file_)
        return cert, key, ca_cert

    def is_admin(self, cert_path):
        """
        Check if certificate is for an admin user.

        Args:
            cert (str): Path to the certficate to check.

        Returns:
            bool: True if admin user.
        """
        cmd = ('openssl x509 -noout -text -in {}'.format(cert_path))
        cert = subprocess.check_output(shlex.split(cmd)).decode('utf-8')
        for line in cert.splitlines():
            match = re.search(r'\s*Subject.*OU *= *(\w*), CN *=.*', line)
            if match:
                return 'akm_admin' in match.group(1)
        return False
