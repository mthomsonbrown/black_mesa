import logging


class Logger:
    """Common configuration and formatting for logging."""
    def __init__(self, tag):
        self._logger = logging.getLogger(tag)

        if self._logger.handlers:
            return

        self._logger.setLevel(logging.DEBUG)

        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s: [%(levelname)s] '
                                      '%(name)s - %(message)s')
        console.setFormatter(formatter)

        self._logger.addHandler(console)

    @property
    def logger(self):
        return self._logger
