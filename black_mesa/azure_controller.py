import warnings

from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.network import NetworkManagementClient
from azure.mgmt.compute import ComputeManagementClient

from black_mesa.logger import Logger
from black_mesa.environment import Environment

# Importing from models is deprecated, but until
# it actually stops working, i don't want to refactor this.
# Ideally all these provisioning controllers will be replaced
# with ansible anyway...
# pylint: disable=ungrouped-imports
with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    from azure.mgmt.compute.models import (
        StorageProfile,
        OSDisk,
        DiskCreateOptionTypes,
        VirtualHardDisk
    )


class AzureController(object):
    """Perform operations on Azure virtual machine resources."""

    def __init__(self):
        self.log = Logger(__name__).logger
        self.log.debug("In Azure Controller!")
        self.env = self.load_config()
        credentials = ServicePrincipalCredentials(
            client_id=self.env['client_id'],
            secret=self.env['service_principal_password'],
            tenant=self.env['tenant_id']
        )
        self.resource_client = ResourceManagementClient(
            credentials,
            str(self.env['subscription_id'])
        )
        self.compute_client = ComputeManagementClient(
            credentials,
            str(self.env['subscription_id'])
        )
        self.network_client = NetworkManagementClient(
            credentials,
            str(self.env['subscription_id'])
        )
        self.instances = []

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'azure': {
                'subscription_id': '<The Azure Subscription ID>',
                'tenant_id': '<The Azure Tenant ID>',
                'client_id': '<Service Principal App ID>',
                'service_principal_password':
                    '<Your Service Principal Password>',
                'location': 'westus',
                'resource_group': 'automation-resources',
                'resource_prefix': 'automation',
                'vm_user': 'azure-user',
                'vm_password': 'OOHXPq6r530N6re',
                'storage_name': 'townsendpackerbuilds',
                'os_disk_name': 'test_disk_name',
                'target_vhd': ('<Name of the vhd file in storage to '
                               'deploy instances with>')
            }
        })
        return env.load_config()['azure']

    def create_instance(self, instance_id=0):
        """
        Create a new Virtual Machine.

        Args:
            instance_id (int): number appended to instance component
                names to distinguish them.
        """
        vm_name = '{}-{}-{}'.format(self.env['resource_prefix'],
                                    'vm', instance_id)
        nic_name = '{}-{}-{}'.format(self.env['resource_prefix'],
                                     'nic', instance_id)
        ip_name = '{}-{}-{}'.format(self.env['resource_prefix'],
                                    'ip', instance_id)
        disk_name = '{}-{}-{}'.format(self.env['resource_prefix'],
                                      'disk', instance_id)

        pub_ip = self.create_ip(ip_name)
        nic = self.create_nic(nic_name, pub_ip.id)
        instance = self.create_vm(vm_name, nic.id, disk_name)
        self.instances.append(instance)
        vm_ip = self.get_ip(instance.name)
        self.log.debug(f"Created Virtual Machine '{instance.name}' with IP '{vm_ip}'")
        return vm_ip

    def get_ip(self, name):
        """
        Retrieve the IP of a VM instance.

        Args:
            name (str): The name of the VM instance.

        Returns:
            str: The IP address.
        """
        instance = self.compute_client.virtual_machines.get(
            self.env['resource_group'], name)
        nic_id = instance.network_profile.network_interfaces[0].id
        nic_name = nic_id.split('/')[-1]
        nic = self.network_client.network_interfaces.get(
            self.env['resource_group'], nic_name)
        ip_name = nic.ip_configurations[0].public_ip_address.id.split('/')[-1]
        sys_ip = self.network_client.public_ip_addresses.get(
            self.env['resource_group'], ip_name)
        return sys_ip.ip_address

    def create_ip(self, ip_name):
        """Create a new IP resource on Azure."""
        self.log.debug(f"Creating IP: {ip_name}")
        poll = self.network_client.public_ip_addresses.create_or_update(
            self.env['resource_group'],
            ip_name,
            {
                'location': self.env['location']
            }
        )
        poll.wait()
        return poll.result()

    def create_nic(self, nic_name, ip_id):
        """Create a Network Interface for a VM."""
        self.log.debug(f"Creating Network Interface: {nic_name}")
        subnet = self.network_client.subnets.get(
            'townsend-akm', 'akm-network', 'default')
        poll = self.network_client.network_interfaces.create_or_update(
            self.env['resource_group'],
            nic_name,
            {
                'location': self.env['location'],
                'ip_configurations': [
                    {
                        'name': 'automation-ip-0',
                        'public_ip_address': {
                            'id': ip_id
                        },
                        'subnet': subnet
                    }
                ]
            }
        )
        poll.wait()
        return poll.result()

    def create_vm(self, vm_name, nic_id, disk_name):
        """Create a Virtual Machine."""
        self.log.debug(f"Creating Virtual Machine: {vm_name}")
        storage_profile = StorageProfile(
            os_disk=OSDisk(
                create_option=DiskCreateOptionTypes.from_image,
                name=self.env['os_disk_name'],
                vhd=VirtualHardDisk(
                    uri='https://{}.blob.core.windows.net/vhds/{}.vhd'.
                    format(self.env['storage_name'], disk_name)
                ),
                os_type='Linux',
                image=VirtualHardDisk(
                    uri='https://{}.blob.core.windows.net/{}/{}'.
                    format(self.env['storage_name'],
                           'system/Microsoft.Compute/Images/vhds',
                           self.env['target_vhd']),
                )
            )
        )

        poll = self.compute_client.virtual_machines.create_or_update(
            self.env['resource_group'],
            vm_name,
            {
                'location': self.env['location'],
                'os_profile': {
                    'computer_name': vm_name,
                    'admin_username': self.env['vm_user'],
                    'admin_password': self.env['vm_password']
                },
                'hardware_profile': {
                    'vm_size': 'Standard_DS1_v2'
                },
                'storage_profile': storage_profile,
                'network_profile': {
                    'network_interfaces': [{
                        'id': nic_id
                    }]
                }
            }
        )
        poll.wait()
        return poll.result()
