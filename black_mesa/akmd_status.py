class AKMDStatus:
    SUCCESS = '0000'
    GET_TEMPLATE_DEPTH_ERROR = '3067'
    NO_ENTRY_FOR_MIRROR_NAME = '3068'
    KEY_HAS_BEEN_REVOKED = '3115'
    CANNOT_OPEN_DATABASE = '3266'
    QUERY_METADATA_ERROR = '3269'
    NO_ENTRY_FOR_KEY_NAME_INSTANCE = '3572'
    READ_USER_ACCESS_ERROR = '3391'
    READ_KEY_ACCESS_ERROR = '3440'
    NO_KEYS_FOUND = '3487'
    AUTO_GEN_KEYS_ERROR = '3552'
    RSA_KEY_NOT_FOUND = '3608'
    READ_GROUP_ACCESS_ERROR = '3610'
    READ_GROUP_MEMBER_ERROR = '3995'
    AUTH_ADMIN_SAME_AS_REQUEST_ADMIN = '4075'
    AUTH_ADMIN_WINDOW_EXPIRED = '4076'
    AUTH_ADMIN_WINDOW_NOT_SET = '4096'
    INVALID_CHARACTER = '4332'
    RSA_NOT_DELETABLE = '4671'
