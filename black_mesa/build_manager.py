import subprocess
import shlex
from pathlib import Path
from abc import ABC, abstractmethod
import shutil
import os
import tarfile

import git

from black_mesa.environment import Environment


class BuildManager(ABC):
    """Base class for creating builders for various packages."""
    def __init__(
            self,
            src_path=None,
            branch_name=None,
            export_path=None,
            hostname=None,
            username=None,
            key=None,
            password=None,
            overwrite=False
    ):
        """
        Set the common config values necessary for a build.

        Args:
            src_path (Path): The local path to the source directory.
            branch_name (str): The name of the branch to use for the build.
            export_path (Path): The directory to copy the package to.
                If left blank, the local workspace will be used.
            hostname (str): The hostname or ip of the system to copy to.
                If left blank, will be set to localhost.
            username (str): The username for the remote system.
            key (Path): If using an ssh key, the local path to that key.
            password (str): The password, if using, to authenticate to the
                remote repository.
        """
        self._name = None
        self.env = self.load_config()['build'][self.project_name]
        workbench = Path(self.load_config()['workbench'])
        self.src_path = src_path or self.env['src_path']
        if not self.src_path:
            raise ValueError(
                "source path either needs to be passed as a parameter or "
                "set in the config file."
            )
        self.src_path = Path(self.src_path)

        self.branch_name = branch_name or self.env['branch_name']

        base_export_dir = Path(
            export_path or self.env['export_path'] or workbench
        )
        self.export_dir = base_export_dir

        self.hostname = hostname or self.env['hostname'] or 'localhost'
        self.username = username or self.env['username']
        self.key = key or self.env['key']
        self.password = password or self.env['password']

        if branch_name:
            self.checkout_branch()

        self.overwrite = overwrite

        self._deb_dir = None
        self._deb_dist = None

    def checkout_branch(self):
        repo = git.Repo(self.src_path)
        repo.git.checkout(self.branch_name)

    @property
    @abstractmethod
    def project_name(self):
        pass

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'build': {
                self.project_name: {
                    'src_path': '',
                    'branch_name': '',
                    'export_path': '',
                    'hostname': '',
                    'username': '',
                    'key': '',
                    'password': ''
                }
            }
        })
        return env.load_config()

    def export_package(self, local_path):
        """
        Upload the package to the specified export location.

        Args:
            local_path (Path): path to local package

        Returns:
            Path: The location of the uploaded item on the remote system.
        """
        export_file = self.export_dir / local_path.name
        if 'localhost' not in self.hostname:
            raise NotImplementedError("Remote upload not yet implemented")

        if export_file.is_file():
            if not self.overwrite:
                raise FileExistsError(
                    "File already exists: {}".format(str(export_file))
                )
            os.remove(str(export_file))
        if not self.export_dir.is_dir():
            os.makedirs(str(self.export_dir))
        shutil.copy(str(local_path), str(export_file))
        return export_file


class PythonDebBuilder(BuildManager):

    @property
    @abstractmethod
    def project_name(self):
        pass

    @property
    def deb_dist(self):
        if self._deb_dist:
            return self._deb_dist
        self._deb_dist = self.src_path / 'deb_dist'
        return self._deb_dist

    @property
    def deb_dir(self):
        if self._deb_dir:
            return self._deb_dir
        deb_dist_dirs = [dir for dir in self.deb_dist.iterdir() if dir.is_dir()]
        assert len(deb_dist_dirs) == 1
        self._deb_dir = deb_dist_dirs[0] / 'debian'
        return self._deb_dir

    @property
    def deb_file(self):
        """Return the path to the deb file."""
        deb_files = [
            deb for deb in self.deb_dist.iterdir() if 'deb' in deb.suffix
        ]
        assert len(deb_files) == 1
        return deb_files[0]

    def stdeb(self):
        """Use stdeb to convert a python package to a debian."""
        subprocess.check_call(
            shlex.split(
                'python3 setup.py '
                '--command-packages=stdeb.command bdist_deb'
            ),
            cwd=str(self.src_path)
        )

    def add_postinst(self):
        """Add the postinst file to the debian directory."""
        shutil.copy(
            str(self.src_path / 'postinst'),
            str(self.deb_dir)
        )

    def add_dependency(self, *debs):
        """
        Add non-python dependencies to the control file.

        Args:
            *debs: The names of the packages to add to the control file.
        """
        control = self.deb_dir / 'control'

        new_debs = ', '.join(debs)
        new_con = []
        with open(str(control), 'r') as p3con:
            for line in p3con.readlines():
                if line.startswith('Depends:'):
                    new_con.append('{}, {}'.format(line.strip(), new_debs))
                    continue
                new_con.append(line.strip())

        with open(str(control), 'w') as p3con:
            p3con.write('\n'.join(new_con))

    def rebuild_deb(self):
        """After modifying files, rebuild the debian package."""
        subprocess.check_call(
            shlex.split(
                'dpkg-buildpackage -rfakeroot -uc -us'
            ),
            cwd=str(self.deb_dir.parent)
        )


class BrapiBuilder(PythonDebBuilder):
    @property
    def project_name(self):
        return 'brapi'

    def add_static_files(self):
        """
        stdeb is not putting files in the manifest into the debian package, so
        for now we'll do that by hand.

        I also tried doing this before the rebuild_deb step, but they still
        weren't included, so putting this in afterwards, with its own
        unpackage/repackage steps...
        """
        proj_init = '{}/__init__.py'.format(self.project_name)
        static_dir = self.src_path / 'static'
        temp = self.deb_dir.parent / 'tmp'
        os.makedirs(str(temp))
        subprocess.check_call(['dpkg-deb', '-R', str(self.deb_file), str(temp)])
        prj_deb_dir = [file_ for file_ in temp.rglob(proj_init)]
        assert len(prj_deb_dir) == 1, "Unexpected project deb dir."
        prj_deb_dir = prj_deb_dir[0].parent
        shutil.copytree(str(static_dir), str(prj_deb_dir / 'static'))
        subprocess.check_call(['dpkg-deb', '-b', str(temp), str(self.deb_file)])

    def collect_static_files(self):
        subprocess.check_call(
            ['python3', 'manage.py', 'collectstatic', '--noinput'],
            cwd=self.src_path
        )

    def build(self):
        """
        Builds brapi
        """
        self.stdeb()
        self.add_postinst()
        self.add_dependency('nginx', 'gunicorn3', 'python3-coreapi')
        self.rebuild_deb()
        self.collect_static_files()
        self.add_static_files()
        return self.export_package(self.deb_file)


class BootstrapBuilder(PythonDebBuilder):
    @property
    def project_name(self):
        return 'bootstrap'

    def build(self):
        """
        Builds bootstrap
        """
        self.stdeb()
        self.add_postinst()
        self.add_dependency('curl')
        self.rebuild_deb()
        return self.export_package(self.deb_file)


class AKM2JSONBuilder(PythonDebBuilder):
    @property
    def project_name(self):
        return 'akm2json'

    def build(self):
        """
        Builds AKM2JSON
        """
        self.stdeb()
        return self.export_package(self.deb_file)


class PlankBuilder(PythonDebBuilder):
    @property
    def project_name(self):
        return 'plank'

    def build(self):
        """
        Builds plank.
        """
        self.stdeb()
        return self.export_package(self.deb_file)


class ASABuilder(PythonDebBuilder):
    @property
    def project_name(self):
        return 'asa'

    def build(self):
        """
        Builds ASA.
        """
        self.stdeb()
        return self.export_package(self.deb_file)


class EKMAdminPackager:

    """
    Create a wheelhouse package of internal dependecies to be installed
    using pip.
    """

    def __init__(self):
        self.env = self.load_config()

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'pkg_whl': {
                self.project_name: {
                    'wheels': [],
                    'export_path': "",
                    'doc_path': "",
                    'req_path': ""
                }
            }
        })
        return env.load_config()['pkg_whl'][self.project_name]

    @property
    def project_name(self):
        return 'ekm_admin'

    def package(self):
        """
        Put the wheels in a tarball.
        """
        wh_path = Path('wheels')
        ex_path = self.get_export_path()
        req_path = self.compile_req_file()
        with tarfile.open(ex_path, "w:gz") as tar:
            tar.add(self.env['doc_path'], arcname=Path(self.env['doc_path']).name)
            tar.add(req_path, arcname=req_path.name)
            for wheel in self.env['wheels']:
                tar.add(wheel, arcname=wh_path / Path(wheel).name)
        print(ex_path.name)

    def get_export_path(self) -> Path:
        """
        Define the filepath for the tarball.
        """
        for wheel in self.env['wheels']:
            wheelpath = Path(wheel)
            if wheelpath.name.startswith(self.project_name):
                ver = self.get_version_string(wheelpath).replace('.', '_')
        parent_path = Path(self.env['export_path'])
        return parent_path / f"{self.project_name}_{ver}.tar.gz"

    def get_version_string(self, wheelpath: Path) -> str:
        """
        Parse version string from a wheel package.

        Args:
            wheelpath: Path to the wheel to get the versions string from.
        """
        return wheelpath.name.split('-')[1]

    def get_wheel_name(self, wheelpath: Path) -> str:
        """
        Parse package name from a wheel package.

        Args:
            wheelpath: Path to the wheel to get the name string from.
        """
        return wheelpath.name.split('-')[0]

    def compile_req_file(self) -> Path:
        """
        Create a requirements file, pointing to the target versions of the wheels.
        """
        req_path = Path(self.env['req_path'])

        with open(req_path, 'w') as req_file:

            for wheel in self.env['wheels']:
                wheelpath = Path(wheel)
                req_file.write(
                    f"{self.get_wheel_name(wheelpath)}"
                    ">="
                    f"{self.get_version_string(wheelpath)}"
                    "\n"
                )
        return req_path
