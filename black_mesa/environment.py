"""Environment.py"""
import os
import json
from pathlib import Path


class Environment(object):
    """
    Handles the environment setup and management for an AKM client.

    This class will look for a config file in the current working directory
    unless an environment variable is set: black_mesa_config.
    """

    def __init__(self):
        self.config_json = {
            'workbench': '<system directory to put temp files>'
        }
        self.config_name = "black_mesa"

        self.config_path = Path(os.getcwd(), self.config_name + '.json')
        if 'black_mesa_config' in os.environ:
            self.config_path = os.getenv('black_mesa_config')
        self.config = {}

    def initialize_system(self):
        """
        Do any system preparation required by config file.

        This will be helper specific, but make sure to call super, as
        Environment requires this method to run.
        """
        if not os.path.isdir(self.config['workbench']):
            os.makedirs(self.config['workbench'])

    def load_config(self):
        """
        Check for a config file on the system.

        If the file doesn't exist or is incomplete, create necessary fields
        and prompt user to add data.
        """
        if not os.path.isfile(self.config_path):
            self.initialize_config_file()
            raise EnvironmentError((
                'No configuration file was detected, so an empty one was made'
                ' for you in {}.  Please fill out the fields and try again.'
                ).format(self.config_path), self.config_path)

        with open(self.config_path) as json_file:
            self.config = json.loads(json_file.read())

        self.validate_config_file()
        self.initialize_system()
        return self.config

    def initialize_config_file(self):
        """Make a stub config file."""
        with open(self.config_path, 'w') as json_file:
            json.dump(
                self.config_json, json_file,
                indent=4, separators=(',', ': ')
            )

    def validate_config_file(self):
        """
        Make sure config file is complete and fields are filled in by user.

        Raises:
            ValueError: If any field in the config json appears to be
                uninitialized.
            KeyError: If a field present in config_json is not present in the
                config file read from disk.
        """
        missing_fields = {}
        stubs = []

        self.find_missing_fields(self.config_json, self.config, missing_fields)
        if missing_fields:
            self.add_missing_config_fields(self.config, missing_fields)
            with open(self.config_path, 'w') as json_file:
                json.dump(
                    self.config, json_file,
                    indent=4, separators=(',', ': ')
                )
        self.fields_are_populated(self.config, stubs)
        if missing_fields and stubs:
            raise KeyError((
                'There were fields missing in the config file. '
                'Stubs have been added to your config file.  '
                'Please update the values and try running the tests again. '
                'Config file: {}, missing: {}').format(
                    self.config_path, missing_fields), self.config_path)
        if stubs:
            raise ValueError(
                ('Some fields in the config file appear to not have been'
                 ' initialized: {}.  Please fix it and try running'
                 ' the tests again: {}').format(stubs, self.config_path),
                self.config_path)

    def fields_are_populated(self, node, stubs):
        """
        Iterate config and make sure no data fields contain stub data.

        Args:
            node (dict): The dictionary to check for stubs.
            stubs (list): A list to populate with all discovered stubs.
        """
        for key, value in node.items():
            if isinstance(value, dict):
                self.fields_are_populated(value, stubs)
            elif isinstance(value, str) and value.startswith('<'):
                stubs.append(key)

    def find_missing_fields(self, reference, subject, missing):
        """
        Make a list of any fields missing from the config file.

        Args:
            reference (dict): Dictionary containing all fields we require.
            subject (dict): The dictionary to analyze.
            missing (dict): Dictionary to populate with the missing fields.
        """
        for key, value in reference.items():
            if isinstance(value, dict):
                if key not in subject:
                    missing[key] = value
                else:
                    missing[key] = {}
                    self.find_missing_fields(
                        reference[key], subject[key], missing[key])
                if not missing[key]:
                    del missing[key]
            elif key not in subject:
                missing[key] = value

    def add_missing_config_fields(self, subject, missing):
        """
        Add a dictionary of fields to another dictionary.

        Args:
            subject (dict): The dictionary to add fields to.
            missing (dict): Dictionary containing fields to add to the subject.
        """
        for key, value in missing.items():
            if not isinstance(value, dict):
                subject[key] = value
            if isinstance(value, dict):
                if key not in subject:
                    subject[key] = value
                self.add_missing_config_fields(
                    subject[key], missing[key]
                )
