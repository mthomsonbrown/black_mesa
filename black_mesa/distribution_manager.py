from subprocess import run
from pathlib import Path

from black_mesa.environment import Environment
from black_mesa.logger import Logger


class DistributionManager:
    """
    Download binaries and repositories for use in testing.
    """
    def __init__(self):
        self.log = Logger(__name__).logger
        self.env = self.load_config()
        self.remote_package_dir = Path(self.env['workbench'], 'remote_packages')
        self.remote_package_dir.mkdir(parents=True, exist_ok=True)

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'dist': {
                'local_packages': [],
                'remote_packages': []
            }
        })
        return env.load_config()

    def get_binaries(self):
        if not self.env['dist']['remote_packages']:
            return self.env['dist']['local_packages']

        for package in self.env['dist']['remote_packages']:
            run(['wget', package, '-P', self.remote_package_dir], check=True)

        return (
            self.env['dist']['local_packages'] +
            self.remote_package_dir.iterdir()
        )
