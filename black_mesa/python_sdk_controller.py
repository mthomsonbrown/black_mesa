"""python_sdk_controller.py"""
import shutil
import argparse
from pathlib import Path

import xml.dom.minidom as minidom
import xml.etree.ElementTree as ET

from akm_sdk.akm_admin import AKMAdmin
from akm_sdk.akm_client import AKMClient

from black_mesa.logger import Logger
from black_mesa.client_controller import ClientController


class PythonSDKController(ClientController):
    """
    Download PKI from an AKM and create an XML file for AKMAdmin or AKMClient.
    """
    def __init__(
            self, ip, username, key=None, password=None, ports=None
    ):
        super().__init__(ip, username, key=key, password=password)
        self.log = Logger(__name__).logger

        self._akm_admin = None
        self._akm_admin2 = None
        self._akm_client = None
        self._akm_enc = None
        self._ports = ports

    @property
    def _admin_port(self):
        if self._ports:
            return self._ports['admin']
        return self.env['admin_port']

    @property
    def _client_port(self):
        if self._ports:
            return self._ports['client']
        return self.env['client_port']

    @property
    def _enc_port(self):
        if self._ports:
            return self._ports['enc']
        return self.env['enc_port']

    @property
    def akm_admin(self):
        """
        Get an instance of the akm admin interface.

        Returns:
            akm_libraries_py.AKMAdmin: Interface to python SDK admin API
        """
        if not self._akm_admin:
            self._akm_admin = AKMAdmin(
                self.akm_name, self.admin_users[0],
                str(Path(self.local_storage, self.akm_name + '.xml')),
                info=True,
                edit=False
            )
        return self._akm_admin

    @property
    def akm_admin2(self):
        """
        Get an instance of the akm admin interface with the second
            admin certificate.

        Returns:
            akm_libraries_py.AKMAdmin: Interface to python SDK admin API
        """
        if not self._akm_admin2:
            self._akm_admin2 = AKMAdmin(
                self.akm_name, self.admin_users[1],
                str(Path(self.local_storage, self.akm_name + '.xml')),
                info=False,
                edit=False
            )
        return self._akm_admin2

    @property
    def akm_client(self):
        """
        Get an instance of the akm client interface.

        Returns:
            akm_libraries_py.AKMClient: Interface to python SDK client API
        """
        if not self._akm_client:
            self._akm_client = AKMClient(
                self.akm_name, self.client_users[0],
                str(Path(self.local_storage, self.akm_name + '.xml')),
                info=False,
                edit=False
            )
        return self._akm_client

    @property
    def akm_enc(self):
        """
        Get an instance of the akm enc user interface.

        Returns:
            akm_libraries_py.AKMClient: Interface to python SDK client API
        """
        if not self._akm_enc:
            self._akm_enc = AKMClient(
                self.akm_name, self.enc_users[0],
                str(Path(self.local_storage, self.akm_name + '.xml')),
                info=False,
                edit=False
            )
        return self._akm_enc

    def compile_xml(self):
        """
        Create the XML config file for use with akm_libraries_py.

        If the user is not an admin, create both user and enc roles for it.
        """
        config = self._seed_config()

        for client in self.iter_clients():
            client_name = (
                client.name if '_' not in client.name else
                client.name.split('_')[1]
            )
            self.move_pems_to_root(client)
            cert, key, ca_cert = self.find_cert_key_ca(client)

            if self.is_admin(Path(self.local_storage, cert)):
                config = self.add_role(
                    config,
                    client_name,
                    self._admin_port,
                    cert,
                    key,
                    ca_cert
                )
                continue

            config = self.add_role(
                config,
                client_name,
                self._client_port,
                cert,
                key,
                ca_cert
            )

            config = self.add_role(
                config,
                f"enc_{client_name}",
                self._enc_port,
                cert,
                key,
                ca_cert
            )

        config_file = self.local_storage / (self.akm_name + '.xml')
        pretty = self.pretty_xml(config)
        with open(str(config_file), 'w') as conf:
            conf.write(pretty)

    def _seed_config(self):
        """
        Create an element tree object to fill with client info.

        Returns:
            ElementTree: The tree to populate.
        """
        config = ET.Element('akm', attrib={'name': self.akm_name})
        ET.SubElement(config, 'home').text = str(self.local_storage)
        ET.SubElement(config, 'hostname').text = self.akm_ip
        ET.SubElement(config, 'protocol').text = 'TLS1.2'
        return config

    def move_pems_to_root(self, root):
        """
        If the PEM files are in a PEM subdirectory, copy them to the
        root directory.

        TODO: Why do I do this? Why not just access them from the pem folder?

        Args:
            root (Path): The name of the directory in local storage to
                start searching from.
        """
        pemfolder = Path(self.local_storage, root, 'PEM')
        if not pemfolder.is_dir():
            return
        for file_ in pemfolder.glob('*.pem'):
            # shutil supports pathlib as of python 3.6
            shutil.copy(str(file_), str(Path(self.local_storage, root)))

    def add_role(self, config, name, port, cert, key, ca_cert):
        """
        Add a role to the xml file.

        Args:
            config (ElementTree): The xml file to add role to.
            name (str): Name of the role.
            port (str): The port the role talks over.
            cert (Path): Location to the client cert.
            key (Path): Location of the client key.
            ca_cert (Path): Location of the certificate authority certificate.
        """
        role = ET.SubElement(config, 'role', attrib={'name': name})
        ET.SubElement(role, 'port').text = str(port)
        ET.SubElement(role, 'cert').text = str(cert)
        ET.SubElement(role, 'key').text = str(key)
        ET.SubElement(role, 'ca').text = str(ca_cert)
        return config

    def pretty_xml(self, config):
        rough_string = ET.tostring(config, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent='  ')


def exec_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'ip', metavar='IP', type=str, help='The IP of the target AKM.')
    parser.add_argument(
        'username', type=str, help='The name of the user on the target AKM.'
    )
    parser.add_argument(
        '--key', help='An SSH key to access the AKM.', default=None)
    args = parser.parse_args()

    client = PythonSDKController(args.ip, args.username, args.key)
    client.download_pki()
    client.compile_xml()


if __name__ == '__main__':
    exec_cli()
