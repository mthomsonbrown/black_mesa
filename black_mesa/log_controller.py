from pathlib import Path


class LogController:

    """
    Perform operations on the akm log files.

    Args:
        akm (AKMController): Controller for the akm to operate on.
        log_path (Path): Path to the file to operate on.
    """

    def __init__(self, akm, log_path):
        self.logrotate_file = Path('/etc/logrotate.d/akm')

        self.akm = akm
        self.log_path = log_path
        self._rotated_path = None

    def calculate_rotated_path(self, date=None):
        """
        Form the anticipated path and file name for the log once rotated.

        Args:
            date (datetime): The date to calculate the path from. If None,
                the current date will be used.

        Returns:
            Path: The anticipated rotated log filepath
        """
        remote = self.akm.remote
        date = date or remote.date()

        logstamp = date.strftime('%Y%m%d')
        rotated_log_name = f'{self.log_path.name}-{logstamp}.gz'
        return self.log_path.parent / rotated_log_name

    @property
    def rotated_path(self):
        """Calculated path and filename for rotated log."""
        if not self._rotated_path:
            self._rotated_path = self.calculate_rotated_path()
        return self._rotated_path

    def fill_file(self, amount=10):
        """
        Fill the log file with random data.

        Args:
            amount (int): The number of megabytes to write to the file.
        """
        self.akm.remote.exec_check(
            f'head -c {amount}M </dev/urandom >{self.log_path}'
        )
        return self

    def rem_log_archive(self):
        """
        Remove the rotated log archive associated with this object's log path
        """
        self.akm.remote.exec_check(
            f'rm -rf {self.rotated_path}'
        )
        return self

    def rotate_logs(self):
        """
        Call logrotate on the akm rotate conf file.
        """
        self.akm.remote.exec_check(
            f'sudo logrotate {self.logrotate_file}'
        )
        return self

    def archive_exists(self):
        """
        Check if the archive associated with this log file exists.

        Returns:
            bool: True if archive exists.
        """
        files = self.akm.remote.exec_check(
            f'ls {self.log_path.parent}'
        ).decode('utf-8')
        print(f"Files is {files}")
        print(f"Rotated path name: {self.rotated_path.name}")
        return self.rotated_path.name in files

    def empty(self):
        """
        Check if the log file is empty.

        Returns:
            bool: True if log file is empty.
        """
        return not self.akm.remote.exec_check(f'cat {self.log_path}')

    def perform_rotation(self):
        """
        Delete current log archive if there is one,
        fill the log file, and call rotate.
        """
        self.fill_file().rem_log_archive().rotate_logs()
        assert self.archive_exists()
