from typing import Tuple, Dict
from pathlib import Path
import json

from polling import poll

from black_mesa.remote_controller import RemoteController
from black_mesa.python_sdk_controller import PythonSDKController
from black_mesa.environment import Environment


class K8SController:

    """
    Perform administrative tasks on a k8s cluster. This class should
    offer similar functionality to the AKMController.
    """

    def __init__(
            self, control_layer: Tuple[RemoteController], worker_nodes: Tuple[RemoteController]
    ):
        self._control_layer = control_layer
        self._worker_nodes = worker_nodes
        self._main = self._control_layer[0]
        self._psc = None
        self.env = self.load_config()['k8s']
        self._wait_for_readiness()
        self._bin_land = Path('/home/ubuntu/binaries/')

    @property
    def python_sdk_controller(self):
        if self._psc:
            return self._psc
        self._psc = PythonSDKController(
            self._main.sys_ip, self._main.username,
            key=self._main.key, password=self._main.password,
            ports=self.get_node_ports()
        )
        self._stage_pki()
        print(f"pki path is: {self.env['pki_path']}")
        self._psc.download_pki(overwrite=True, path=self.env['pki_path'])
        self._psc.compile_xml()
        return self._psc

    def load_config(self):
        env = Environment()
        env.config_json.update({
            'k8s': {
                'service_name': 'akm',
                'pki_path': str(Path(f'/home/{self._main.username}/pki/'))
            }
        })
        return env.load_config()

    def get_node_ports(self) -> Dict[str, str]:
        """
        Find out what akm ports have been mapped to what node ports.

        Args:
            master: A session into the master node of a k8s cluster.
            service: Name of the service to query

        Returns:
            A dict with akm ports for keys and node ports for values.
        """
        info = self._main.exec_check(
            f'kubectl get svc {self.env["service_name"]} -o json',
            json_out=True
        )
        ports = {}
        for port in info['spec']['ports']:
            ports[port['name']] = port['nodePort']
        return ports

    def install_local_package(self, path: Path):
        """
        Install a package located on the test system to the akm pod on a running k8s cluster.

        Args:
            path: The path to the local package.
        """
        self._main.upload(path, self._bin_land)
        self._main.exec_check(f'kubectl cp {self._bin_land / path.name} akm:/home/admin/uploads/')
        self._main.exec_check(
            f'kubectl exec -it akm -- sudo apt-get install -y /home/admin/uploads/{path.name}',
            ignore='Unable to use a TTY'
        )

    def exec(self, cmd):
        """
        Run a command on the master node.
        """
        return self._main.exec_check(cmd, ignore='Unable to use a TTY')

    def _wait_for_readiness(self):
        """
        Check the akm pod for readiness state.
        """

        def ready() -> bool:
            """
            Check if the ready status is true
            """
            info = self._main.exec_check('kubectl get pod akm -o json')
            info = json.loads(info.decode('utf-8'))
            for condition in info['status']['conditions']:
                if condition['type'] == 'Ready':
                    if condition['status'] == 'True':
                        return True
            return False

        timeout = 60 * 5
        print(f"Waiting {timeout} seconds for readiness")
        poll(
            ready,
            step=1,
            timeout=timeout
        )

    def _stage_pki(self):
        """
        Copy pki from the akm pod to the master node.
        """
        self._main.exec_check(f'kubectl cp akm:/home/admin/downloads/ {self.env["pki_path"]}')
