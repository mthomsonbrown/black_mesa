import os
import shutil
import time

from pathlib import Path

from black_mesa.logger import Logger
from black_mesa.environment import Environment
from black_mesa.remote_controller import RemoteController
from black_mesa.python_sdk_controller import PythonSDKController
from black_mesa.akmadminc_controller import AKMAdminCController
from black_mesa.legacy_bootstrap_controller import LegacyBootstrapController
from black_mesa.bootstrap_controller import BootstrapController
from black_mesa.p2j_controller import P2JController


class AKMController(object):
    def __init__(self, ip: str, key: str = None, password: str = None, ports: dict = None):
        """
        Interact with a remote AKM environment.

        Args:
            ip (str): The IP address of the target system.
            key (str): Filepath to the private key used for authentication.  If
                not set, password will be used from the config file.
        """
        self.log = Logger(__name__).logger
        self.env = self.load_config()['akm']

        self._sys_ip = ip
        self.username = self.env['username']
        self.key = key
        self.password = password
        self.ports = ports

        self.api_path = os.path.join(self.env['bootstrap_path'], 'api')

        self.remote = RemoteController(
            ip, self.env['username'], key=key, password=password, timeout=3
        )

        self.sftp = self.remote.ssh.open_sftp()
        self._name = None
        self.local_storage = None
        self.conn = None

        self._psc = None
        self._aac = None
        self._lbc = None
        self._bsc = None
        self._p2j = None

    @property
    def name(self):
        """hostname of the akm"""
        if not self._name:
            self._name = self.remote.name
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def python_sdk_controller(self) -> PythonSDKController:
        """
        Provision an python sdk controller for this AKM.
        """
        if self._psc:
            return self._psc
        self._psc = PythonSDKController(
            self._sys_ip, self.username, key=self.key, password=self.password, ports=self.ports
        )
        self._psc.download_pki()
        self._psc.compile_xml()
        return self._psc

    @property
    def akmadminc_controller(self):
        """
        Provision an akmadminc controller for this AKM.

        Returns:
            AKMAdminCController
        """
        if self._aac:
            return self._aac
        self._aac = AKMAdminCController(
            self._sys_ip, self.username, key=self.key, password=self.password
        )
        self._aac.download_pki()
        self._aac.upload_pki(self._aac.local_storage)
        self._aac.compile_conf()
        self._aac.upload_conf()

        return self._aac

    @property
    def legacy_bootstrap_controller(self):
        """
        Provision a legacy bootstrap controller for this AKM.

        Returns:
            LegacyBootstrapController
        """
        if self._lbc:
            return self._lbc
        self._lbc = LegacyBootstrapController(
            self.remote, self.api_path
        )
        return self._lbc

    @property
    def bootstrap_controller(self):
        """
        Provision a bootstrap controller for this AKM.

        Returns:
            BootstrapController
        """
        if self._bsc:
            return self._bsc
        self._bsc = BootstrapController(self.remote)
        return self._bsc

    @property
    def json_logger(self):
        """
        Provision a json log controller.

        Returns:
            P2JController
        """
        if self._p2j:
            return self._p2j
        self._p2j = P2JController(self.remote)
        return self._p2j

    @property
    def ip(self):
        """Show system IP."""
        return self._sys_ip

    def load_config(self):
        env = Environment()
        username = 'admin'
        tmp = env.load_config()
        env.config_json.update({
            'akm': {
                'username': username,
                'password': 'OOHXPq6r530N6re',
                'bootstrap_path': os.path.join(
                    os.sep, 'var', 'lib', 'townsend', 'akm', 'bootstrap'
                ),
                'uploads_path': os.path.join(
                    os.sep, 'home', username, 'uploads'
                ),
                'akmdb_tool_path': os.path.join(
                    os.sep, 'var', 'lib', 'townsend', 'akm', 'akmdb_tool'
                ),
                'local_pki': os.path.join(tmp['workbench'], 'akm_pki'),
                'blockchain_path': os.path.join(
                    os.sep, 'home', username, 'blockchain'
                ),
                'ssh_pubkey_path': os.path.join(
                    os.sep, 'home', username, '.ssh', 'id_rsa.pub'
                ),
                'local_ssh_pubkey_path': os.path.join(tmp['workbench']),
                'database_path': os.path.join(
                    os.sep, 'var', 'lib', 'townsend', 'akm', 'db', 'akm.db'
                ),
                'conf_file_path': os.path.join(
                    os.sep, 'etc', 'akm', 'akm.conf'
                ),
                'log_dir': os.path.join(
                    os.sep, 'var', 'log', 'townsend'
                )
            }
        })
        return env.load_config()

    def rotate_logs(self):
        """
        Rotate the logs in the log directory.
        """
        self.remote.exec_check("sudo chown root:root /etc/logrotate.d/akm")
        self.remote.exec_check("sudo logrotate /etc/logrotate.d/akm")
        for file_ in self.remote.list_dir('/var/log/townsend/'):
            if '.gz' in Path(file_).suffix:
                return
        raise IOError(
            'Expected a log archive after rotation, but none was present.'
        )

    def download_ssh_pubkey(self):
        """
        Pull root ca cert from akm and store on test system.

        Returns:
            Path: The location of the downloaded cert.
        """
        self.log.debug("Downloading RootCACertificate...")
        return self.remote.download(
            Path(self.env['ssh_pubkey_path']),
            Path(self.env['local_ssh_pubkey_path'])
        )

    def turn_off_akmd(self):
        """
        Turn off akmd process.  Return when process has stopped.
        """
        timeout = 5
        frequency = 0.5
        time_stop = time.time() + timeout
        self.remote.exec_check('sudo killall akmd')
        while time.time() < time_stop:
            running = False
            pids = self.remote.exec_check('ps -A')
            for pid in pids.decode('utf-8').split('\n'):
                if 'akmd' in pid:
                    running = True
                    time.sleep(frequency)
                    break
            if not running:
                return

        raise IOError("AKMD did not shut off in {} seconds".format(timeout))

    def turn_on_akmd(self):
        """
        Turn on akmd process.  Return when process has started.
        """
        timeout = 5
        frequency = 0.5
        time_stop = time.time() + timeout
        self.remote.exec_check('sudo service akm start')
        while time.time() < time_stop:
            pids = self.remote.exec_check('ps -A')
            for pid in pids.decode('utf-8').split('\n'):
                if 'akmd' in pid:
                    return
            time.sleep(frequency)
        raise IOError("AKMD did not start in {} seconds".format(timeout))

    def cleanup(self):
        """Remove any local files.  Turn on AKM if it's off."""
        if not self.local_storage:
            return
        self.log.debug("Removing local PKI")
        if os.path.exists(self.local_storage):
            shutil.rmtree(self.local_storage)

    def install_bootstrap_api(self, source_path):
        """
        Upload and configure bootstrap API.

        Update:
            Mystery to me. Why make the init file?

        Args:
            api_path (str): Path to the local api folder to deploy.
        """
        self.log.debug("Installing bootstrap API")
        self.remote.upload(source_path, Path(self.api_path))
        self.remote.exec_check('touch {}'.format(os.path.join(
            self.env['bootstrap_path'], '__init__.py')))

    def upload_file(self, source, contents=False):
        """
        Copy a file to the akm's upload directory.

        Args:
            source (Path): The path of the file to upload.
            contents (bool): If true upload the contents of the source
                directory as loose files in akm's uploads directory.
        """
        if not contents:
            self.log.debug(f"Uploading file to AKM: {source}")
            self.remote.upload(
                source,
                Path(self.env['uploads_path'], source.name)
            )
            return
        for file in source.iterdir():
            self.upload_file(file)

    def download_blockchain(self, path):
        """
        Get the blockchain repo from client system.

        Args:
            path (str): The location of the blockchain repo on the client.
        """
        self.log.debug("Downloading blockchain repo.")
        self.remote.upload(path, self.env['blockchain_path'])

    def hide_database(self):
        """
        Move the database from its expected location.
        """
        self.remote.exec_check(
            'sudo mv {dbp} {dbp}.bak'.format(dbp=self.env['database_path'])
        )

    def unhide_database(self):
        """
        Move the database back to its expected location.
        """
        self.remote.exec_check(
            'sudo mv {dbp}.bak {dbp}'.format(dbp=self.env['database_path'])
        )

    def set_dual_knowledge(self, value):
        """
        Change the dual knowledge setting in the config file.

        Args:
            value (str): The value to set (Y/N).
        """
        self.edit_config_file('DualKnowledgeRequired', value)

    def set_pcidss_mode(self, value):
        """
        Change the pcidss mode setting in the config file.

        Args:
            value (str): The value to set (Y/N).
        """
        self.edit_config_file('PCIDSSMode', value)

    def get_pcidss_mode(self):
        """
        Read the pcidss mode from the config file.

        Returns:
            str: The current mode value.
        """
        return self.get_config_value('PCIDSSMode')

    def edit_config_file(self, key, value):
        """
        Change a line in the config file.

        Args:
            key (str): The text in the config file to search for.
            value (str): The value to set for the key.
        """
        if key == "TrustedCACert":
            raise NotImplementedError(
                'TrustedCACert can be duplicated in the config '
                'file and this method does not yet support that.'
            )
        conf_file_path = self.env['conf_file_path']
        self.turn_off_akmd()
        conf_lines = self.remote.exec_check(
            f'cat {conf_file_path}'
        )

        conf_lines = conf_lines.decode('utf-8').strip().split('\n')
        for index, line in enumerate(conf_lines):
            if line.startswith(key):
                conf_lines[index] = f'{key}={value}'

        new_conf = '\n'.join(conf_lines)
        self.remote.exec_check(
            f'echo "{new_conf}" > {conf_file_path}'
        )

        self.turn_on_akmd()

    def get_config_value(self, key):
        """
        Read a value from the config file.

        Args:
            key (str): The config option to return the value of.

        Returns:
            str: The value associated with the key.
        """
        conf_file_path = self.env['conf_file_path']
        conf_lines = self.remote.exec_check(
            f'cat {conf_file_path}'
        )

        conf_lines = conf_lines.decode('utf-8').split('\n')
        for index, line in enumerate(conf_lines):
            if line.startswith(key):
                value = conf_lines[index].split('=')[1]
                return value
        raise ValueError(f"{key} not found in {conf_file_path}")

    def install_package(self, path):
        """
        Upload a debian package to the AKM and install it.

        Args:
            path (Path): The location of the package on the local system.
        """
        package = self.remote.upload(path, Path('/home/admin'))
        wait_time = 45
        self.wait_for_dpkg_lock(wait_time)
        self.remote.exec_check('sudo dpkg -i {}'.format(package))

    def apt_install_package(self, path):
        """
        Upload a debian package to the AKM and install it using apt.  This
        calls apt in non-interactive mode and will install dependencies the
        package has listed.

        Args:
            path (Path): The location of the package on the local system.
        """
        package = self.remote.upload(path, Path('/home/admin'))
        self.wait_for_dpkg_lock()
        self.log.info(f"Installing {path.name}")
        try:
            self.remote.exec_check(
                'sudo DEBIAN_FRONTEND=noninteractive '
                f'apt-get -yq install {package}',
                # ignore='WARNING'
            )
        except OSError:
            self.log.info("Installation failed. Trying again...")
            time.sleep(5)
            self.remote.exec_check(
                'sudo DEBIAN_FRONTEND=noninteractive '
                f'apt-get -yq install {package}',
                # ignore='WARNING'
            )

    def wait_for_dpkg_lock(self, timeout=60 * 5):
        """
        Check the dpkg lock file.  Return when free, or raise exception
        if timeout reached.

        Args:
            timeout (float): Seconds to wait before giving up.
        """
        self.log.info("Waiting up to %s seconds for dpkg lock...", timeout)
        polling_frequency = 0.5
        time_stop = time.time() + timeout
        while time.time() < time_stop:
            _, stdout, _ = self.remote.ssh.exec_command(
                "ps aux | grep '[a]pt'"
            )
            stuff = stdout.readlines()
            if not stuff:
                return
            time.sleep(polling_frequency)
        raise IOError(f"Timeout limit reached.  dpkg lock not released: {stuff}")
